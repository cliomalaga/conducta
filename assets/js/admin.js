var cantidad = 10;
var imagenEliminada = false;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$('body').on('click', 'nav>ul>li',
        function() {
              var category = nameCorrect($(this).find('a').html());
              show(category);
        });
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function show(_category) {
      document.getElementsByTagName('section')[0].innerHTML = '<img id="loading" src="assets/img/loading.gif" alt=""/>';
      document.getElementsByTagName('footer')[0].style.display = "none";
      var url = 'sectionsAdmin/' + _category + 'admin.php';

      $.ajax({
            type: "POST",
            url: url,
            data: {
                  'category': _category,
                  'contador': 0,
                  'cantidad': cantidad
            },
            beforeSend: function() {
                  if (document.getElementById("mas") !== null) {// Si el boton "mas" ya existe
                        document.getElementById("mas").remove();
                  }
            },
            success: function(data) {
                  var data = $.parseJSON(data);
                  document.getElementsByTagName('section')[0].innerHTML = data.form;
                  document.getElementsByTagName('section')[0].innerHTML += data.html;
                  document.getElementsByTagName('main')[0].innerHTML += data.input;
                  animateDivs();
            }
      });
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function nameCorrect(name) {
      name = name.replace('á', 'a');
      name = name.replace('é', 'e');
      name = name.replace('í', 'i');
      name = name.replace('ó', 'o');
      name = name.replace('ú', 'u');
      name = name.replace('º', '');
      return name;
}
/////////////////////////////////////// ANIMATE DIV /////////////////////////////////////////////////////////////////
function animateDivs() {
      var divs = document.getElementsByClassName('animate');
      var intervalo = setInterval(function() {
            animate();
      }, 100);
      var ndiv = 0;
      function animate() {
            if (divs[ndiv] == null) {
                  clearInterval(intervalo);
            } else {
                  divs[ndiv].style.display = "block";
                  $(divs[ndiv]).addClass('bounceInUp animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                        $(this).removeClass('bounceInUp animated');
                        if (document.getElementById("mas")) {
                              document.getElementById('mas').style.display = "block";
                        }
                  });
                  if (ndiv == divs.length - 1) {
                        clearInterval(intervalo);
                  }
                  ndiv++;
            }
      }
}
//////////////////////////////////////////// VIEW MORE ////////////////////////////////////////////////////////////////////
function mostrarMas(category, contador) {
      var url = 'sectionsAdmin/' + category + 'admin.php';
      $.ajax({
            type: "POST",
            url: url,
            data: {
                  'category': category,
                  'contador': contador,
                  'cantidad': cantidad
            },
            success: function(data) {
                  var data = $.parseJSON(data);

                  if (document.getElementById("mas") !== null) {// Si el boton "mas" ya existe
                        document.getElementById("mas").remove();
                  }
                  document.getElementsByTagName('section')[0].innerHTML += data.html;
                  document.getElementsByTagName('main')[0].innerHTML += data.input;
                  animateDivs();
            }
      });
}
///////////////////////////////////////// OPEN ANSWER //////////////////////////////////////////////////////////////////////////
$("body").on("click", ".arrows",
        function() {
              var id = '';
              if ($(this).attr("id").length == 6)
                    id = $(this).attr("id").substring(5, 6);
              if ($(this).attr("id").length == 7)
                    id = $(this).attr("id").substring(5, 7);
              if ($(this).attr("id").length == 8)
                    id = $(this).attr("id").substring(5, 8);
              var arrow = document.getElementById('arrow' + id);
              var content = this.parentNode.parentNode.lastElementChild;
              if (content.style.display == 'none') {
                    $(content).slideDown("slow");
                    arrow.src = "assets/img/menos.png";
              } else {
                    $(content).slideUp("slow");
                    arrow.src = "assets/img/mas.png";
              }
        });
/////////////////////////////////////////// VIEW PDF////////////////////////////////////////////////////////////////////////
$("body").on("click", ".leerMas",
        function() {
              document.getElementById("divPdf").innerHTML = '<object id="object" data="' + this.name + '"></object>';
              document.getElementById("divPdf").innerHTML += '<input type="image" src="assets/img/close.png" id="close">';
              $('#divPdf').fadeIn();
              $('#divOscuro').fadeIn();
              document.body.style.overflowY = "hidden";
        });
/////////////////////////////////////////// CLOSE PDF////////////////////////////////////////////////////////////////////////
$("body").on("click", "#close",
        function() {
              $('#divPdf').fadeOut();
              $('#divOscuro').fadeOut();
              document.body.style.overflowY = "scroll";
        });
/////////////////////////////////////////// BUTTON ADD ////////////////////////////////////////////////////////////////////////
$("body").on("click", "#butAdd",
        function() {

              var formData = new FormData(document.getElementById('formulario'));
              var category = document.getElementById('subject').getAttribute("data-id");
              formData.append("category", category);

              $.ajax({
                    type: "POST",
                    url: "assets/php/create.php",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                          document.getElementsByTagName('section')[0].innerHTML = '<img id="loading" src="assets/img/loading.gif" alt=""/>';
                          document.getElementsByTagName('footer')[0].style.display = "none";
                          if (document.getElementById("mas")) {
                                document.getElementById('mas').style.display = "none";
                          }
                    },
                    success: function(data) {
                          if (data != '') {
                                modal(data);
                          } else {
                                modal("Agregado correctamente");
                          }
                          show(category);
                    }
              });
        });

/////////////////////////////////////////// FUNCION ELIMINAR ////////////////////////////////////////////////////////////////////////
var divAborrar;

$("body").on("click", ".eliminar",
        function() {
              var category = document.getElementById('subject').getAttribute("data-id");
              var id = this.parentNode.getAttribute("data-id");
              divAborrar = this.parentNode.parentNode;
              modalConfirmacion('OKBorrar', id, category);
        });

$("body").on("click", "#OKBorrar",
        function() {
              var category = this.getAttribute("data-category");
              var id = this.getAttribute("data-id");
              $.ajax({
                    type: "POST",
                    url: "assets/php/delete.php",
                    data: {'category': category, 'id': id},
                    success: function(data) {
                          $('#divModal').fadeOut();
                          $('#divOscuro').fadeOut();
                          document.body.style.overflowY = "scroll";
                          $(divAborrar).fadeOut();
                    }
              });
        });
/////////////////////////////////////////// FUNCION FORMULARIO EDITAR ////////////////////////////////////////////////////////////////////////
$("body").on("click", ".editar",
        function() {
              var articulo = this.parentNode.parentNode;
              articulo.innerHTML = "";
              var category = document.getElementById('subject').getAttribute("data-id");
              var id = this.parentNode.getAttribute("data-id");
              var buttonsEdit = document.getElementsByClassName('editar');
              var buttonsDelete = document.getElementsByClassName('eliminar');
              for (i = 0; limite = buttonsEdit.length, i < limite; i++) {
                    buttonsEdit[i].disabled = true;
                    buttonsDelete[i].disabled = true;
              }

              $.ajax({
                    type: "POST",
                    url: "assets/php/formEdit.php",
                    data: {'category': category, 'id': id},
                    beforeSend: function() {
                          articulo.style.textAlign = "center";
                          articulo.innerHTML = '<img id="loading" src="assets/img/loading.gif" alt="" style="margin:0 auto;"/>';
                          document.getElementsByTagName('footer')[0].style.display = "none";
                    },
                    success: function(data) {
                          articulo.style.textAlign = "left";

                          articulo.innerHTML = data;
                    }
              });
        });

/////////////////////////////////////////// PREVIEW IMAGE ////////////////////////////////////////////////////////////////////////
$("body").on("change", "#subirImagen",
        function(evt) {
              document.getElementById('divImgPrevia').innerHTML = '';
              var files = evt.target.files;
              if (files.length != 0) {
                    if (files[0]['type'] == 'image/jpeg' || files[0]['type'] == 'image/png' || files[0]['type'] == 'image/gif' || files[0]['type'] == 'image/bmp') {
                          file = files[0];
                          leerArchivo = new FileReader();
                          leerArchivo.onload = (function(e) {
                                return function(e) {
                                      document.getElementById('divImgPrevia').innerHTML = '<img class="imgPrevia" src="' + e.target.result + '" alt=""/>';
                                };
                          })(file);
                          leerArchivo.readAsDataURL(file);
                    } else {
                          modal("Imagen no válida");
                    }
              }
        });
/////////////////////////////////////////// PREVIEW IMAGE EDITION ////////////////////////////////////////////////////////////////////////
$("body").on("change", "#subirImagenEdicion",
        function(evt) {
              document.getElementById('divImgPreviaEdicion').innerHTML = '';
              var files = evt.target.files;
              if (files.length != 0) {
                    if (files.length != 0 && (files[0]['type'] == 'image/jpeg' || files[0]['type'] == 'image/png' || files[0]['type'] == 'image/gif' || files[0]['type'] == 'image/bmp')) {
                          file = files[0];
                          leerArchivo = new FileReader();
                          leerArchivo.onload = (function(e) {
                                return function(e) {
                                      document.getElementById('divImgPreviaEdicion').innerHTML = '<img class="imgPrevia" src="' + e.target.result + '" alt=""/>';
                                };
                          })(file);
                          leerArchivo.readAsDataURL(file);
                    } else {
                          modal("Imagen no válida");
                    }
              }
        });
/////////////////////////////////////////// CLEAN IMAGE ////////////////////////////////////////////////////////////////////////
$("body").on("click", "#divImgPrevia",
        function() {
              modalConfirmacion('OKBorrarImagen');
        });
$("body").on("click", "#divImgPreviaEdicion",
        function() {
              var id = this.parentNode.getAttribute("data-id");
              var category = this.parentNode.parentNode.parentNode.getAttribute("data-id");
              modalConfirmacion('OKBorrarImagenEdicion', id, category);
        });

$("body").on("click", "#OKBorrarImagen",
        function() {
              document.getElementById('divImgPrevia').innerHTML = '';
              var input = $("#subirImagen");
              input.replaceWith(input.val('').clone(true));
              cerrarModal();
        });

$("body").on("click", "#OKBorrarImagenEdicion",
        function() {
              imagenEliminada = true;
              document.getElementById('divImgPreviaEdicion').innerHTML = '';
              var input = $("#subirImagenEdicion");
              input.replaceWith(input.val('').clone(true));
              cerrarModal();
        });
/////////////////////////////////////////// UPLOAD PDF ////////////////////////////////////////////////////////////////////////
$("body").on("change", "#subirPDF",
        function(evt) {
              console.log("pi");
              console.log(evt.target.files[0]['type']);
              console.log(evt);
              if (evt.target.files[0]['type'] == 'application/pdf' || evt.target.files[0]['type'] == 'application/x-rar'|| evt.target.files[0]['type'] == 'application/download') {
                    this.previousSibling.innerHTML = "PDF cargado";
              } else {
                    modal("PDF no válido");
              }
        });
$("body").on("change", "#subirPDFEdicion",
        function(evt) {
              console.log("pi");
              console.log(evt.target.files[0]['type']);
              if (evt.target.files[0]['type'] == 'application/pdf' || evt.target.files[0]['type'] == 'application/x-rar' || evt.target.files[0]['type'] == 'application/download') {
                    this.previousSibling.innerHTML = "PDF cargado";
              } else {
                    modal("PDF no válido");
              }
        });

/////////////////////////////////////////// FUNCION CANCELUPDATE ////////////////////////////////////////////////////////////////////////
$("body").on("click", "#butCancelUpdate",
        function() {
              var category = document.getElementById('subject').getAttribute("data-id");
              show(category);
        });

/////////////////////////////////////////// FUNCION UPDATE ////////////////////////////////////////////////////////////////////////
$("body").on("click", "#butUpdate",
        function() {

              var formData = new FormData(document.getElementById('formularioEdicion'));
              var category = document.getElementById('subject').getAttribute("data-id");
              var id = this.parentNode.parentNode.getAttribute("data-id");
              var subcategory = document.getElementById("selCategoriasEdicion").value;

              formData.append("category", category);
              formData.append("id", id);
              formData.append("subcategory", subcategory);
              formData.append("imagenEliminada", imagenEliminada);
              var articulo = this.parentNode.parentNode.parentNode;
              articulo.innerHTML = "";
              $.ajax({
                    type: "POST",
                    url: "assets/php/update.php",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                          articulo.style.textAlign = "center";
                          articulo.innerHTML = '<img id="loading" src="assets/img/loading.gif" alt="" style="margin:0 auto;"/>';
                          document.getElementsByTagName('footer')[0].style.display = "none";
                    },
                    success: function(data) {
                          var data = $.parseJSON(data);
                          articulo.style.textAlign = "left";
                          articulo.innerHTML = data.html;
                          if (data.error != "") {
                                modal(data.error);
                          }
                          var buttonsEdit = document.getElementsByClassName('editar');
                          var buttonsDelete = document.getElementsByClassName('eliminar');
                          for (i = 0; limite = buttonsEdit.length, i < limite; i++) {
                                buttonsEdit[i].disabled = false;
                                buttonsDelete[i].disabled = false;
                          }
                          imagenEliminada = false;
                    }
              });
        });

/////////////////////////////////////////// CLEAN PLACEHOLDER ////////////////////////////////////////////////////////////////////////
$("body").on("focus", "#formulario textarea",
        function() {
              var placeholder = this.placeholder;
              this.placeholder = '';
              $("body").on("blur", "#formulario textarea",
                      function() {
                            this.placeholder = placeholder;
                      });
        });
$("body").on("focus", "#formulario input[type='text']",
        function() {
              var placeholder = this.placeholder;
              this.placeholder = '';
              $("body").on("blur", "#formulario input[type='text']",
                      function() {
                            this.placeholder = placeholder;
                      });
        });
$("body").on("focus", "#formularioEdicion textarea",
        function() {
              var placeholder = this.placeholder;
              this.placeholder = '';
              $("body").on("blur", "#formulario textarea",
                      function() {
                            this.placeholder = placeholder;
                      });
        });
$("body").on("focus", "#formularioEdicion input[type='text']",
        function() {
              var placeholder = this.placeholder;
              this.placeholder = '';
              $("body").on("blur", "#formulario input[type='text']",
                      function() {
                            this.placeholder = placeholder;
                      });
        });
/////////////////////////////////////////// FUNCION MODAL ////////////////////////////////////////////////////////////////////////

function modal(_message) {
      document.getElementById('divModal').innerHTML = "<h1><span>" + _message + "</span></h1><input id='cerrarModal' type='button' value='OK'/>";
      $('#divModal').fadeIn();
      $('#divOscuro').fadeIn();
      document.body.style.overflowY = "hidden";
}

function modalConfirmacion(_id, _nid, _category) {
      document.getElementById('divModal').innerHTML = "<h1><span>¿Seguro que quiere borrar?</span></h1>" +
              "<input id='" + _id + "' type='button' value='OK' data-category='" + _category + "' data-id='" + _nid + "'/>" +
              "<input id='cerrarModal' type='button' value='Cancelar'/>";
      $('#divModal').fadeIn();
      $('#divOscuro').fadeIn();
      document.body.style.overflowY = "hidden";
}
/////////////////////////////////////////// CLOSE MODAL////////////////////////////////////////////////////////////////////////
$("body").on("click", "#cerrarModal",
        function() {
              cerrarModal();
        });

function cerrarModal() {
      $('#divModal').fadeOut();
      $('#divOscuro').fadeOut();
      document.body.style.overflowY = "scroll";
}