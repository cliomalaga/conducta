var cantidad = 15;

//////////////////////////////////////////// WINDOW ONLOAD ////////////////////////////////////////////////////////////////////
window.onload = function() {
      viewIndex();
};
//////////////////////////////////////////// CLICK SECTION ////////////////////////////////////////////////////////////////////
$('body').on('click', 'nav>ul>li>ul>li',
        function() {

              document.getElementsByTagName('section')[0].innerHTML = '<img id="loading" src="assets/img/loading.gif" alt=""/>';
              document.getElementsByTagName('footer')[0].style.display = "none";
              var category = nameCorrect($(this).parents(':eq(1)').find('a').html());
              var subcategory = nameCorrect($(this).find('a').html());
              var contador = '';
              if (subcategory == "Visual") {
                    cantidad = 1000;
              }
              var url = 'sections/' + category + '.php';
              $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                          'category': category,
                          'subcategory': subcategory,
                          'contador': contador,
                          'cantidad': cantidad
                    },
                    beforeSend: function() {
                          if (document.getElementById("mas") !== null) {// Si el boton "mas" ya existe
                                document.getElementById("mas").remove();
                          }
                    },
                    success: function(data) {
                          var data = $.parseJSON(data);
                          document.getElementsByTagName('section')[0].innerHTML = data.html;
                          document.getElementsByTagName('main')[0].innerHTML += data.input;
                          animateDivs();
                          hideshowAds();
                          if (subcategory == "Visual") { //Si es de la subcategoria visual solo sacamos la imagenes sin contenedor
                                $(".rslides").responsiveSlides({
                                      auto: false,
                                      nav: true,
                                });
                          }
                    }
              });
        });
//////////////////////////////////////////// VIEW MORE ////////////////////////////////////////////////////////////////////

function mostrarMas(category, subcategory, contador) {
      var url = 'sections/' + category + '.php';
      $.ajax({
            type: "POST",
            url: url,
            data: {
                  'category': category,
                  'subcategory': subcategory,
                  'contador': contador,
                  'cantidad': cantidad
            },
            success: function(data) {
                  var data = $.parseJSON(data);
                  if (document.getElementById("mas") !== null) {// Si el boton "mas" ya existe
                        document.getElementById("mas").remove();
                  }
                  document.getElementsByTagName('section')[0].innerHTML += data.html;
                  document.getElementsByTagName('main')[0].innerHTML += data.input;
                  animateDivs();
                  hideshowAds();
            }
      });
}
/////////////////////////////////////// ANIMATE DIV /////////////////////////////////////////////////////////////////
function animateDivs() {
      var divs = document.getElementsByClassName('animate');
      var intervalo = setInterval(function() {
            animate();
      }, 200);
      var ndiv = 0;
      function animate() {
            if (divs[ndiv] != null) {
                  divs[ndiv].style.display = "block";
                  $(divs[ndiv]).addClass('bounceInUp animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                        $(this).removeClass('bounceInUp animated');
                        if (document.getElementById("mas")) {
                              document.getElementById('mas').style.display = "block";
                        }
                  });
            }
            if (divs[ndiv] == null || ndiv == divs.length - 1) {
                  // if (ndiv == divs.length - 1) {
                  clearInterval(intervalo);
                  document.getElementsByTagName('footer')[0].style.display = "block";
                  $('footer').addClass('bounceInUp animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                        $(this).removeClass('bounceInUp animated');
                  });
            }
            $(this).removeClass('bounceInUp animated');
            ndiv++;
      }
}
///////////////////////////////////////// OPEN ANSWER //////////////////////////////////////////////////////////////////////////
$("body").on("click", ".arrows",
        function() {
              var id = '';
              if ($(this).attr("id").length == 6)
                    id = $(this).attr("id").substring(5, 6);
              if ($(this).attr("id").length == 7)
                    id = $(this).attr("id").substring(5, 7);
              if ($(this).attr("id").length == 8)
                    id = $(this).attr("id").substring(5, 8);
              var arrow = document.getElementById('arrow' + id);
              var content = this.parentNode.parentNode.lastElementChild;
              if (content.style.display == 'none') {
                    $(content).slideDown("slow");
                    arrow.src = "assets/img/menos.png";
              } else {
                    $(content).slideUp("slow");
                    arrow.src = "assets/img/mas.png";
              }
        });

/////////////////////////////////////////// VIEW PDF////////////////////////////////////////////////////////////////////////
$("body").on("click", ".leerMas",
        function() {
              document.getElementById("divPdf").innerHTML = '<object id="object" data="' + this.name + '"></object>';
              document.getElementById("divPdf").innerHTML += '<input type="image" src="assets/img/close.png" id="close">';
              $('#divPdf').fadeIn();
              $('#divOscuro').fadeIn();
              document.body.style.overflowY = "hidden";
        });
/////////////////////////////////////////// CLOSE PDF////////////////////////////////////////////////////////////////////////
$("body").on("click", "#close",
        function() {
              $('#divPdf').fadeOut();
              $('#divOscuro').fadeOut();
              document.body.style.overflowY = "scroll";
        });
/////////////////////////////////////////// VIEW NAME ////////////////////////////////////////////////////////////////////////
$("body").on("mouseover", ".fotoProfesor",
        function() {
              var datos = this.getAttribute("data-id").split('-');
              document.getElementById("divDatos").innerHTML = '<strong>' + datos[0] + '</strong><br>' + datos[1];
        });
////////////////////////////////////////////  SEARCH /////////////////////////////////////////////////////////////////
$("body").on("keyup", "#txtBuscar",
        function() {
              // Delay onkeyup event #txtBuscar
              var cadena = document.getElementById("txtBuscar").value;
              console.log("txtBuscar->" + cadena);

              clearTimeout($.data(this, 'timer'));
              var wait = setTimeout("search('" + cadena + "')", 500);
              $(this).data('timer', wait);
        });

function search(_cadena) {
      if (_cadena.length == 0) {
            viewIndex();
      } else if (_cadena.length > 2) {
            $.ajax({
                  type: "POST",
                  url: "assets/php/search.php",
                  data: {'cadena': _cadena},
                  success: function(data) {
                        document.getElementsByTagName('section')[0].innerHTML = data;
                        console.log("data" + data);
                        animateDivs();
                        hideshowAds();
                  }
            });
      }
}
document.getElementById('txtBuscar').addEventListener('blur', function() {
      document.getElementById("txtBuscar").value = '';
}, false);
////////////////////////////////////////////// VIEW INDEX /////////////////////////////////////////////////////////////////////
function viewIndex() {
      window.document.getElementsByTagName('section')[0].innerHTML = "<div id='divIzquierda'></div><div id='divDerecha'></div>";
      document.getElementById('divIzquierda').style.overflow = "visible";
      document.getElementById('divDerecha').style.overflow = "visible";
      document.getElementById('divIzquierda').style.height = "auto";
      document.getElementById('divDerecha').style.height = "auto";

      $.ajax({
            type: "POST",
            url: 'sections/Articulos.php',
            data: {
                  'category': 'Articulos',
                  'subcategory': '%_',
                  'contador': 0,
                  'cantidad': 10
            },
            success: function(data) {
                  var data = $.parseJSON(data);
                  document.getElementById('divIzquierda').innerHTML += data.html;
                  $(".elemento").addClass("elementoIndex");
                  $(".elemento").removeClass("elemento");
                  $.ajax({
                        type: "POST",
                        url: 'sections/Noticias.php',
                        data: {
                              'category': 'Noticias',
                              'subcategory': '%_',
                              'contador': 0,
                              'cantidad': 6
                        },
                        success: function(data) {
                              var data = $.parseJSON(data);
                              document.getElementById('divDerecha').innerHTML += data.html;
                              $(".elemento").addClass("elementoIndex");
                              $(".elemento").removeClass("elemento");
                              $.ajax({
                                    type: "POST",
                                    url: 'sections/Libros.php',
                                    data: {
                                          'category': 'Libros',
                                          'subcategory': '%_',
                                          'contador': 0,
                                          'cantidad': 6
                                    },
                                    success: function(data) {
                                          var data = $.parseJSON(data);
                                          document.getElementById('divDerecha').innerHTML += data.html;
                                          $(".elemento").addClass("elementoIndex");
                                          $(".elemento").removeClass("elemento");
                                          animateDivs();
                                          hideshowAds()
                                    }
                              });
                        }
                  });
            }
      });
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function nameCorrect(name) {
      name = name.replace('á', 'a');
      name = name.replace('é', 'e');
      name = name.replace('í', 'i');
      name = name.replace('ó', 'o');
      name = name.replace('ú', 'u');
      name = name.replace('º', '');
      return name;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function hideshowAds() {
      function height() {
            var altura = $('#sectionSection').height();
            console.log(altura);
            if (altura < 600) {
                  document.getElementsByClassName('adsbygoogle')[0].style.display = "block";
                  document.getElementsByClassName('adsbygoogle')[1].style.display = "none";
                  document.getElementsByClassName('adsbygoogle')[2].style.display = "none";
            } else if (altura < 1200) {
                  document.getElementsByClassName('adsbygoogle')[0].style.display = "block";
                  document.getElementsByClassName('adsbygoogle')[1].style.display = "block";
                  document.getElementsByClassName('adsbygoogle')[2].style.display = "none";
            } else {
                  document.getElementsByClassName('adsbygoogle')[0].style.display = "block";
                  document.getElementsByClassName('adsbygoogle')[1].style.display = "block";
                  document.getElementsByClassName('adsbygoogle')[2].style.display = "block";
            }
      }
      setTimeout(height, 2000);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*PARA ANULAR BOTON DERECHO
 $(document).ready(function() {
 $(document).bind("contextmenu", function(e) {
 return false;
 });
 });*/


