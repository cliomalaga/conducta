////////////////////////////////////////////// MAIL TO /////////////////////////////////////////////////////////////////////
$("body").on("click", ".comentario",
	function () {
		$('#divOscuro').css("display", "block");
		$('#divComentario').css("display", "block");
		$('#divComentario').addClass('zoomIn animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
			$(this).removeClass('zoomIn animated');
		});
		document.getElementById("texto").focus();
	});
/////////////////////////////////// VALIDATE MAIL //////////////////////////////////////////////////////////
function validarNombre() {
	var nombre = document.getElementById("txtNombre").value;
	var expNombre = /^[a-zA-ZñÑáéíóú\'\s]{2,50}$/;
	if (expNombre.test(nombre) == false) {
		return false;
	} else {
		return true;
	}
}

function pintarNombre() {
	if (validarNombre() || document.getElementById("txtNombre").value == '') {
		document.getElementById("warningNombre").innerHTML = '';
		document.getElementById("warningNombre").style.opacity = 0;
	} else {
		document.getElementById("warningNombre").innerHTML = 'Nombre no válido';
		document.getElementById("warningNombre").style.opacity = 1;
	}
}

function validarEmail() {
	var email = document.getElementById("txtEmail").value;
	var expEmail = /^[a-zA-Z]+@{1}[a-zA-Z]+\.{1}[a-zA-Z]+$/;
	if (expEmail.test(email) == false) {
		return false;
	} else {
		return true;
	}
}

function pintarEmail() {
	if (validarEmail() || document.getElementById("txtEmail").value == '') {
		document.getElementById("warningCorreo").innerHTML = '';
		document.getElementById("warningCorreo").style.opacity = 0;

	} else {
		document.getElementById("warningCorreo").innerHTML = 'Email no válido';
		document.getElementById("warningCorreo").style.opacity = 1;
	}
}

function validarFormPreguntas() {
	var boton = document.getElementById("butEnviarMail");
	var check = document.getElementById("checkAceptar").checked;

	if (validarEmail() && validarNombre() && check == true)
		boton.disabled = false;
	else
		boton.disabled = true;
}

$("body").on("click", "#butEnviarMail",
	function () {
		var mensaje = texto.value;
		var subject = document.getElementById('subject').getAttribute("data-id");
		var nombre = document.getElementById('txtNombre').value;
		var email = document.getElementById('txtEmail').value;
		$.ajax({
			type: "POST",
			url: "assets/php/email.php",
			data: {
				'subject': subject,
				'mensaje': mensaje,
				'nombre': nombre,
				'email': email
			},
			success: function (data) {
				$('#divComentario').css("display", "none");
				resetearForm();
				modal("Mensaje enviado correctamente");
			}
		});
	});
//////////////////////////////////////// CLOSE MAIL ///////////////////////////////////////////////////////////////////////////
$("body").on("click", ".cancelar",
	function () {
		resetearForm();
		$('#divComentario').addClass('zoomOut animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
			$('#divComentario').removeClass('zoomOut animated');
			$('#divComentario').css("display", "none");
		});
		$('#divOscuro').css("display", "none");
		document.body.style.overflowY = "scroll";
	});
/////////////////////////////////////////// FUNCION MODAL ////////////////////////////////////////////////////////////////////////
function modal(_message) {
	document.getElementById('divModal').innerHTML = "<h1><span>" + _message + "</span></h1><input id='cerrarModal' type='button' value='OK'/>";
	$('#divModal').fadeIn();
	$('#divOscuro').fadeIn();
	document.body.style.overflowY = "hidden";
}

/////////////////////////////////////////// CLOSE MODAL////////////////////////////////////////////////////////////////////////
$("body").on("click", "#cerrarModal",
	function () {
		cerrarModal()
	});

function cerrarModal() {
	$('#divModal').fadeOut();
	$('#divOscuro').fadeOut();
	document.body.style.overflowY = "scroll";
}
/////////////////////////////////////////// RESETEAR FORMULARIO////////////////////////////////////////////////////////////////////////
function resetearForm() {
	document.getElementById("txtNombre").value = '';
	document.getElementById("txtEmail").value = '';
	document.getElementById("texto").value = '';
	document.getElementById("checkAceptar").checked = false;
	document.getElementById("warningNombre").innerHTML = '';
	document.getElementById("warningCorreo").innerHTML = '';
	document.getElementById("butEnviarMail").disabled = true;
}