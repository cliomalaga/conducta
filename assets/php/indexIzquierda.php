<?php

require 'conection.php';

$stmt = $oConni->prepare("SELECT TITULO,RESUMEN,REFERENCIA, RUTA_REFERENCIA,RUTA_IMAGEN,RUTA_PDF FROM NOTICIA WHERE ID_CATEGORIA IN (SELECT ID_CATEGORIA FROM NOTICIA_CATEGORIA) ORDER BY ID_NOTICIA DESC LIMIT 0,6");
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($titulo, $resumen, $referencia, $ruta_referencia, $ruta_imagen, $ruta_pdf);

$vacio = true;
if ($stmt->num_rows > 0)
      $vacio = false;
echo '<div id="subject" data-id="Noticias">'; //Abrimos div SUBJECT
while ($stmt->fetch()) {
      echo '<div class="elemento animate">'; //Abrimos div NOTICIAS
      echo '<div class="divTitulo">'; //Abrimos div TITULO

      if ($titulo != '') {
            echo '<div>' . utf8_decode($titulo) . '</div>';
      }
      if ($ruta_pdf != '') {
            echo '<a class="descargarPdf" href="../../assets/pdf/' . $ruta_pdf . '" download="' . $ruta_pdf . '"></a>';
      }
      echo '<img src="../../assets/img/mail.png" class="thumbs comentario"/>';
      echo '</div>'; //Cerramos div TITULO
      if ($referencia != '' && $ruta_referencia != '') {//div REFERENCIA
            echo '<div class="divReferencia"><a href="http://' . $ruta_referencia . '" target="_blank">' . utf8_decode($referencia) . '</a></div>';
      }
      echo '<br>';

      if ($ruta_imagen != '') {
            echo '<div class="divImagen"><img src="../../assets/images/' . $ruta_imagen . '" class="img"></div>';
      }

      if ($resumen != '') {
            echo '<div class="divResumen">' . nl2br(utf8_decode($resumen)) . '</div>'; //Div RESUMEN
      }

      if ($ruta_pdf != '') {
            echo '<div class="divLeer"><a class="leerMas" name="../../assets/pdf/' . $ruta_pdf . '" title="Leer más sobre ' . $ruta_pdf . '">Leer más...</a></div>';
      }
      echo '<br></div>'; //Cerramos div NOTICIAS
}
echo '</div>';
$stmt->close();


/* * ************************************************************** */


$stmt = $oConni->prepare("SELECT TITULO,AUTOR,EDITORIAL,RESUMEN,RUTA_IMAGEN,RUTA_PDF FROM LIBRO WHERE ID_CATEGORIA IN (SELECT ID_CATEGORIA FROM LIBRO_CATEGORIA) ORDER BY ID_LIBRO DESC LIMIT 0,6");
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($titulo, $autor, $editorial, $resumen, $ruta_imagen, $ruta_pdf);

$vacio = true;
if ($stmt->num_rows > 0)
      $vacio = false;
echo '<div id="subject" data-id="Libros">'; //Abrimos div SUBJECT
while ($stmt->fetch()) {
      echo '<div class="elemento animate">'; //Abrimos div LIBROS
      echo '<div class="divTitulo">'; //Abrimos div TITULO

      if ($titulo != '') {
            echo '<div>' . utf8_decode($titulo) . '</div>';
      }

      if ($ruta_pdf != '') {
            echo '<a class="descargarPdf" href="../../assets/pdf/' . $ruta_pdf . '" download="' . $ruta_pdf . '"></a>';
      }
      echo '<img src="../../assets/img/mail.png" class="thumbs comentario"/>';
      echo '</div>'; //Cerramos div TITULO

      echo '<div class="divAutor">'; //Abrimos div AUTOR/EDITORIAL
      if ($autor != '') {
            echo '<strong>Autor:</strong> ' . utf8_decode($autor);
      }
      if ($editorial != '') {
            echo ' - Editorial: ' . utf8_decode($editorial);
      }
      echo '</div><br>'; //Cerramos div AUTOR/EDITORIAL

      if ($ruta_imagen != '') {
            echo '<div class="divImagen"><img src="../../assets/images/' . $ruta_imagen . '" class="img"></div>';
      }

      if ($resumen != '') {
            echo '<div class="divResumen">' . nl2br(utf8_decode($resumen)) . '</div>'; //Div RESUMEN
      }

      if ($ruta_pdf != '') {
            echo '<div class="divLeer"><a class="leerMas" name="../../assets/pdf/' . $ruta_pdf . '" title="Leer más sobre ' . $ruta_pdf . '">Leer más...</a></div>';
      }
      echo '<br></div>'; //Cerramos div LIBROS
}
echo '</div>';
$stmt->close();



