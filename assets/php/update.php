<?php

require "conection.php";

$category = $_POST["category"];
$subcategory = $_POST["subcategory"];
$id = $_POST['id'];
$directorio = '/var/www/assets/';
$ext = array('bmp', 'gif', 'jpg', 'jpeg', 'png', 'ico');
$error = "";
$imagenEliminada = $_POST["imagenEliminada"];

if (isset($_POST["txtTitulo"])) {
      $titulo = stripslashes(utf8_encode($_POST['txtTitulo']));
}
if (isset($_POST["txtAutor"])) {
      $autor = stripslashes(utf8_encode($_POST['txtAutor']));
}
if (isset($_POST["txtFecha"])) {
      $fecha = stripslashes(utf8_encode($_POST['txtFecha']));
}
if (isset($_POST["txtResumen"])) {
      $resumen = stripslashes(utf8_encode($_POST['txtResumen']));
}
if (isset($_POST["txtReferencia"])) {
      $referencia = stripslashes(utf8_encode($_POST['txtReferencia']));
}
if (isset($_POST["txtRutaReferencia"])) {
      $ruta_referencia = stripslashes(utf8_encode($_POST['txtRutaReferencia']));
}
if (isset($_POST["txtEditorial"])) {
      $editorial = stripslashes(utf8_encode($_POST['txtEditorial']));
}
if (isset($_POST["txtRespuesta"])) {
      $respuesta = stripslashes(utf8_encode($_POST['txtRespuesta']));
}


if (isset($_FILES["subirImagenEdicion"]["tmp_name"])) {
      if (is_uploaded_file($_FILES["subirImagenEdicion"]["tmp_name"])) {
            if ($_FILES["subirImagenEdicion"]["size"] < 5000000) {
                  $e = pathinfo($_FILES["subirImagenEdicion"]["name"]);
                  if (array_search($e["extension"], $ext)) { //Si es una de las extensiones que permitimos en el array
                        $html = "entra";
                        $nameImagen = $_FILES["subirImagenEdicion"]["name"];
                        $tmpImagen = $_FILES["subirImagenEdicion"]["tmp_name"];
                        $urlNueva = $directorio . "images/" . $nameImagen;
                        $html.=$tmpImagen . ' - ' . $urlNueva;
                        copy($tmpImagen, $urlNueva);
                  } else {
                        $error = "Imagen no válida";
                  }
            } else {
                  $error = "Imagen demasiado grande";
            }
      } else {
            if ($_POST["imagenEliminada"] == "true") {
                  $nameImagen = "";
            } if ($_POST["imagenEliminada"] == "false") {
                  $nameImagen = $_POST["txtRutaImagen"];
            }
      }
}
if (isset($_FILES["subirPDFEdicion"]["tmp_name"])) {
      if (is_uploaded_file($_FILES["subirPDFEdicion"]["tmp_name"])) {
            if ($_FILES["subirPDFEdicion"]["size"] < 50000000) {
                  if ($_FILES["subirPDFEdicion"]["type"] == "application/pdf" || $_FILES["subirPDFEdicion"]["type"] == "application/x-rar" || $_FILES['subirPDFEdicion']['type'] =="application/download") {
                        $namePdf = $_FILES["subirPDFEdicion"]["name"];
                        $tmpPdf = $_FILES["subirPDFEdicion"]["tmp_name"];
                        $urlNueva = $directorio . "pdf/" . $namePdf;
                        copy($tmpPdf, $urlNueva);
                  } else {
                        $error = "PDF no válido";
                  }
            } else {
                  $error = "PDF demasiado grande";
            }
      } else
            $namePdf = $_POST["txtRutaPdf"];
}

switch ($_POST["category"]) {
      case "Noticias":
            $cSQL = "UPDATE NOTICIA SET TITULO=?, RESUMEN=?, RUTA_IMAGEN=?, RUTA_PDF=?, REFERENCIA=?, RUTA_REFERENCIA=?, ID_CATEGORIA=? WHERE ID_NOTICIA = ?";
            $stmt = $oConni->prepare($cSQL);
            $stmt->bind_param("ssssssii", $titulo, $resumen, $nameImagen, $namePdf, $referencia, $ruta_referencia, $subcategory, $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->close();

            $html = "<div class='divTitulo' data-id='" . $id . "'>";
            if ($titulo != "") {
                  $html.="<strong>" . utf8_decode($titulo) . "</strong>";
            }
            $html.="<input type='image' id='eliminar' name='eliminar' src='assets/img/eliminar.png' value='' class='eliminar thumbs'/>";
            $html.="<input type='image' id='editar' name='editar' src='assets/img/editar.png' value='' class='editar thumbs'/>";
            $html.="</div>";

            if ($referencia != "" && $ruta_referencia != "") { //Div REFERENCIA
                  $html .="<div class='divAutor'><a href='http://" . $ruta_referencia . "' target='_blank'>" . utf8_decode($referencia) . "</a></div>";
            }
            $html.="<br>";

            if ($nameImagen != "") {//Div IMAGEN
                  $html .= "<div class='divImagen'><img src='assets/images/" . $nameImagen . "' class='img'></div>";
            }

            if ($resumen != "") {
                  $html .="<div class='divResumen'>" . nl2br(utf8_decode($resumen)) . "</div>"; //Div RESUMEN
            }

            if ($namePdf != "") {
                  $html.="<div class='divLeer'><a class='leerMas' name='assets/pdf/" . $namePdf . "' title='Leer más sobre " . $namePdf . "'>Leer más...</a></div>";
            }
            $html.="<br>";
            break;
      case "Articulos":
            $monthNames = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
            if (!preg_match('^\d{1,2}\/\d{1,2}\/\d{4}$^', $fecha)) {
                  $fecha = '';
            }

            $cSQL = "UPDATE ARTICULO SET TITULO=?, AUTOR=?, FECHA_PUBLICACION=?, RESUMEN=?, RUTA_IMAGEN=?, RUTA_PDF=?, REFERENCIA=?, RUTA_REFERENCIA=?, ID_CATEGORIA=? WHERE ID_ARTICULO = ?";
            $stmt = $oConni->prepare($cSQL);
            $stmt->bind_param("ssssssssii", $titulo, $autor, $fecha, $resumen, $nameImagen, $namePdf, $referencia, $ruta_referencia, $subcategory, $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->close();

            $html = "<div class='divTitulo' data-id='" . $id . "'>";
            if ($titulo != "") {
                  $html.="<strong>" . utf8_decode($titulo) . "</strong>";
            }
            $html.="<input type='image' id='eliminar' name='eliminar' src='assets/img/eliminar.png' value='' class='eliminar thumbs'/>";
            $html.="<input type='image' id='editar' name='editar' src='assets/img/editar.png' value='' class='editar thumbs'/>";
            $html.="</div>";

            $html .="<div class='divAutor'>"; //Abrimos div AUTOR
            if ($autor != "") {
                  $html.="<strong>Autor:</strong> " . utf8_decode($autor);
            }

            if ($fecha != "") {
                  $month = (substr($fecha, 3, 2) - 1);
                  $fecha = substr($fecha, 0, 2) . " de " . $monthNames[$month] . " de " . substr($fecha, 6, 4);
                  $html.="<br>Publicado el " . $fecha;
            }

            if ($referencia != "" && $ruta_referencia != "") { //Div REFERENCIA
                  $html .=" - (Extraido de <a href='http://" . $ruta_referencia . "' target='_blank'>" . utf8_decode($referencia) . "</a>)";
            }
            $html.="</div>"; //Cerramos div AUTOR
            $html.="<br>";

            if ($nameImagen != "") {//Div IMAGEN
                  $html .= "<div class='divImagen'><img src='assets/images/" . $nameImagen . "' class='img'></div>";
            }

            if ($resumen != "") {
                  $html .="<div class='divResumen'>" . nl2br(utf8_decode($resumen)) . "</div>"; //Div RESUMEN
            }

            if ($namePdf != "") {
                  $html.="<div class='divLeer'><a class='leerMas' name='assets/pdf/" . $namePdf . "' title='Leer más sobre " . $namePdf . "'>Leer más...</a></div>";
            }
            $html.="<br>";

            break;
      case "Libros":
            $cSQL = "UPDATE LIBRO SET TITULO=?, AUTOR=?, EDITORIAL=?, RESUMEN=?, RUTA_IMAGEN=?, RUTA_PDF=?, ID_CATEGORIA=? WHERE ID_LIBRO = ?";
            $stmt = $oConni->prepare($cSQL);
            $stmt->bind_param("ssssssii", $titulo, $autor, $editorial, $resumen, $nameImagen, $namePdf, $subcategory, $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->close();

            $html = "<div class='divTitulo' data-id='" . $id . "'>";
            if ($titulo != "") {
                  $html.="<strong>" . utf8_decode($titulo) . "</strong>";
            }
            $html.="<input type='image' id='eliminar' name='eliminar' src='assets/img/eliminar.png' value='' class='eliminar thumbs'/>";
            $html.="<input type='image' id='editar' name='editar' src='assets/img/editar.png' value='' class='editar thumbs'/>";
            $html.="</div>";

            $html .="<div class='divAutor'>"; //Abrimos div AUTOR
            if ($autor != "") {
                  $html.="<strong>Autor:</strong> " . utf8_decode($autor);
            }

            if ($editorial != "") {
                  $html.=" - Editorial: " . $editorial;
            }
            $html.="</div>"; //Cerramos div AUTOR
            $html.="<br>";

            if ($nameImagen != "") {//Div IMAGEN
                  $html .= "<div class='divImagen'><img src='assets/images/" . $nameImagen . "' class='img'></div>";
            }

            if ($resumen != "") {
                  $html .="<div class='divResumen'>" . nl2br(utf8_decode($resumen)) . "</div>"; //Div RESUMEN
            }

            if ($namePdf != "") {
                  $html.="<div class='divLeer'><a class='leerMas' name='assets/pdf/" . $namePdf . "' title='Leer más sobre " . $namePdf . "'>Leer más...</a></div>";
            }
            $html.="<br>";
            break;
      case "Vida cotidiana":
            $cSQL = "UPDATE VIDA_COTIDIANA SET TITULO=?, AUTOR=?, RESUMEN=?, RUTA_IMAGEN=?, ID_CATEGORIA=? WHERE ID_VIDA_COTIDIANA = ?";
            $stmt = $oConni->prepare($cSQL);
            $stmt->bind_param("ssssii", $titulo, $autor, $resumen, $nameImagen, $subcategory, $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->close();

            $html = "<div class='divTitulo' data-id='" . $id . "'>";
            if ($titulo != "") {
                  $html.="<strong>" . utf8_decode($titulo) . "</strong>";
            }
            $html.="<input type='image' id='eliminar' name='eliminar' src='assets/img/eliminar.png' value='' class='eliminar thumbs'/>";
            $html.="<input type='image' id='editar' name='editar' src='assets/img/editar.png' value='' class='editar thumbs'/>";
            $html.="</div>";

            if ($autor != "") {
                  $html.="<div class='divAutor'><strong>Autor:</strong> " . utf8_decode($autor) . "</div>";
            }
            $html.="<br>";

            if ($nameImagen != "") {//Div IMAGEN
                  $html .= "<div class='divImagen'><img src='assets/images/" . $nameImagen . "' class='img'></div>";
            }

            if ($resumen != "") {
                  $html .="<div class='divResumen'>" . nl2br(utf8_decode($resumen)) . "</div>"; //Div RESUMEN
            }

            $html.="<br>";
            break;
      case "Preguntas":
            $cSQL = "UPDATE PREGUNTAS SET TITULO=?, RESPUESTA=?, ID_CATEGORIA=? WHERE ID_PREGUNTAS = ?";
            $stmt = $oConni->prepare($cSQL);
            $stmt->bind_param("ssii", $titulo, $respuesta, $subcategory, $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->close();


            $html = "<p class='titlePregunta' data-id='" . $id . "'>";
            if ($titulo != "") {
                  $html .= "<img id='arrow" . $id . "' class='arrows' src='assets/img/mas.png'/> " . utf8_decode($titulo);
            }
            $html.="<input type='image' id='eliminar' name='eliminar' src='assets/img/eliminar.png' value='' class='eliminar thumbs'/>";
            $html.="<input type='image' id='editar' name='editar' src='assets/img/editar.png' value='' class='editar thumbs'/></p>";
            if ($respuesta != "") {
                  $html .= "<div class='divRespuesta' style='display:none;'><br>" . nl2br(utf8_decode($respuesta)) . "</div>";
            }

            break;
      case "Bibliografia":
            $cSQL = "UPDATE BIBLIOGRAFIA SET TITULO=?, AUTOR=?, RESUMEN=?, RUTA_IMAGEN=?, RUTA_PDF=?, ID_CATEGORIA=? WHERE ID_BIBLIOGRAFIA = ?";
            $stmt = $oConni->prepare($cSQL);
            $stmt->bind_param("sssssii", $titulo, $autor, $resumen, $nameImagen, $namePdf, $subcategory, $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->close();

            $html = "<div class='divTitulo' data-id='" . $id . "'>";
            if ($titulo != "") {
                  $html.="<strong>" . utf8_decode($titulo) . "</strong>";
            }
            $html.="<input type='image' id='eliminar' name='eliminar' src='assets/img/eliminar.png' value='' class='eliminar thumbs'/>";
            $html.="<input type='image' id='editar' name='editar' src='assets/img/editar.png' value='' class='editar thumbs'/>";
            $html.="</div>";

            if ($autor != "") {
                  $html.="<div class='divAutor'><strong>Autor:</strong> " . utf8_decode($autor) . "</div>";
            }
            $html.="<br>";

            if ($nameImagen != "") {//Div IMAGEN
                  $html .= "<div class='divImagen'><img src='assets/images/" . $nameImagen . "' class='img'></div>";
            }

            if ($resumen != "") {
                  $html .="<div class='divResumen'>" . nl2br(utf8_decode($resumen)) . "</div>"; //Div RESUMEN
            }

            if ($namePdf != "") {
                  $html.="<div class='divLeer'><a class='leerMas' name='assets/pdf/" . $namePdf . "' title='Leer más sobre " . $namePdf . "'>Leer más...</a></div>";
            }
            $html.="<br>";
            break;
      case "Humor":
            $cSQL = "UPDATE HUMOR SET TITULO=?, RESUMEN=?, RUTA_IMAGEN=?, ID_CATEGORIA=? WHERE ID_HUMOR = ?";
            $stmt = $oConni->prepare($cSQL);
            $stmt->bind_param("sssii", $titulo, $resumen, $nameImagen, $subcategory, $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->close();

            $html = "<div class='divTitulo' data-id='" . $id . "'>";
            if ($titulo != "") {
                  $html.="<strong>" . utf8_decode($titulo) . "</strong>";
            }
            $html.="<input type='image' id='eliminar' name='eliminar' src='assets/img/eliminar.png' value='' class='eliminar thumbs'/>";
            $html.="<input type='image' id='editar' name='editar' src='assets/img/editar.png' value='' class='editar thumbs'/>";
            $html.="</div><br>";

            if ($nameImagen != "") {//Div IMAGEN
                  $html .= "<div class='divImagen'><img src='assets/images/" . $nameImagen . "' class='img'></div>";
            }

            if ($resumen != "") {
                  $html .="<div class='divResumen'>" . nl2br(utf8_decode($resumen)) . "</div>"; //Div RESUMEN
            }
            $html.="<br>";
            break;
}

echo json_encode(array("html" => $html, "error" => $error));

