<?php

require 'conection.php';

$category = $_POST['category'];
$id = $_POST['id'];
switch ($category) {
      case 'Noticias':
            $stmt = $oConni->prepare("SELECT * FROM NOTICIA WHERE ID_NOTICIA=?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id_noticia, $titulo, $resumen, $referencia, $ruta_referencia, $ruta_imagen, $ruta_pdf, $categoria);

            if ($stmt->fetch()) {
                  $stmt2 = $oConni2->prepare("SELECT * FROM NOTICIA_CATEGORIA");
                  $stmt2->execute();
                  $stmt2->store_result();
                  $stmt2->bind_result($id_categoria, $nombre);

                  $select = '<label for="selCategorias">Categoría:</label><select id="selCategoriasEdicion" name="selCategoriasEdicion">';
                  while ($stmt2->fetch()) {
                        if ($id_categoria == $categoria) {
                              $select .= '<option value="' . $id_categoria . '" selected>' . $nombre . '</option>';
                        } else {
                              $select .= '<option value="' . $id_categoria . '">' . $nombre . '</option>';
                        }
                  }
                  $select .= '</select>';
                  $stmt2->close();
                  $form = '<form id="formularioEdicion" name="formularioEdicion" method="POST" enctype="multipart/form-data" accept-charset="UTF-8" data-id="' . $id . '">';
                  $form.=$select;
                  $form.='<div class="divInputFile"><span>Subir Imagen</span><input type="file" id="subirImagenEdicion" name="subirImagenEdicion" class="upload"/></div>';
                  $form.='<div class="divInputFile"><span>Subir PDF</span><input type="file" id="subirPDFEdicion" name="subirPDFEdicion" class="upload"/></div><br>';
                  $form.='<div id="divImgPreviaEdicion"><img class="imgPrevia" src="assets/images/' . $ruta_imagen . '"/></div>';
                  $form.='<label for="txtTitulo">Titulo:</label><input type="text" id="txtTitulo" name="txtTitulo" value="' . utf8_decode($titulo) . '"/><br>';
                  $form.='<label for="txtReferencia">Referencia:</label><input type="text" id="txtReferencia" name="txtReferencia" value="' . utf8_decode($referencia) . '"/><br>';
                  $form.='<label for="txtRutaReferencia">Ruta Referencia:</label><input type="text" id="txtRutaReferencia" name="txtRutaReferencia" value="' . utf8_decode($ruta_referencia) . '"/><br>';
                  $form.='<div style="clear:both"><label for="txtResumen">Resumen:</label><textarea id="txtResumen" name="txtResumen">' . utf8_decode($resumen) . '</textarea></div>';

                  $form.='<div style="text-align:center;"><input type="button" id="butUpdate" value="Guardar">';
                  $form.='<input type="button" id="butCancelUpdate" value="Cancelar"></div>';
                  $form.='<input type="hidden" id="txtRutaImagen" name="txtRutaImagen" value="' . $ruta_imagen . '">';
                  $form.='<input type="hidden" id="txtRutaPdf" name="txtRutaPdf" value="' . $ruta_pdf . '">';
                  $form.='</form>';
            }
            $stmt->close();
            break;
      case 'Articulos':
            $stmt = $oConni->prepare("SELECT * FROM ARTICULO WHERE ID_ARTICULO=?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id_articulo, $titulo, $autor, $fecha, $resumen, $ruta_imagen, $ruta_pdf, $referencia, $ruta_referencia, $categoria);

            if ($stmt->fetch()) {
                  $stmt2 = $oConni2->prepare("SELECT * FROM ARTICULO_CATEGORIA");
                  $stmt2->execute();
                  $stmt2->store_result();
                  $stmt2->bind_result($id_categoria, $nombre);

                  $select = '<label for="selCategorias">Categoría:</label><select id="selCategoriasEdicion" name="selCategoriasEdicion">';
                  while ($stmt2->fetch()) {
                        if ($id_categoria == $categoria) {
                              $select .= '<option value="' . $id_categoria . '" selected>' . $nombre . '</option>';
                        } else {
                              $select .= '<option value="' . $id_categoria . '">' . $nombre . '</option>';
                        }
                  }
                  $select .= '</select>';
                  $stmt2->close();
                  $form = '<form id="formularioEdicion" name="formularioEdicion" method="POST" enctype="multipart/form-data" accept-charset="UTF-8" data-id="' . $id . '">';
                  $form.=$select;
                  $form.='<div class="divInputFile"><span>Subir Imagen</span><input type="file" id="subirImagenEdicion" name="subirImagenEdicion" class="upload"/></div>';
                  $form.='<div class="divInputFile"><span>Subir PDF</span><input type="file" id="subirPDFEdicion" name="subirPDFEdicion" class="upload"/></div><br>';
                  $form.='<div id="divImgPreviaEdicion"><img class="imgPrevia" src="assets/images/' . $ruta_imagen . '"/></div>';
                  $form.='<label for="txtTitulo">Titulo:</label><input type="text" id="txtTitulo" name="txtTitulo" value="' . utf8_decode($titulo) . '"/><br>';
                  $form.='<label for="txtAutor">Autor:</label><input type="text" id="txtAutor" name="txtAutor" value="' . utf8_decode($autor) . '"/><br>';
                  $form.='<label for="txtFecha">Fecha de publicación:</label><input type="text" id="txtFecha" name="txtFecha" value="' . $fecha . '"><br>';
                  $form.='<label for="txtReferencia">Referencia:</label><input type="text" id="txtReferencia" name="txtReferencia" value="' . utf8_decode($referencia) . '"/><br>';
                  $form.='<label for="txtRutaReferencia">Ruta Referencia:</label><input type="text" id="txtRutaReferencia" name="txtRutaReferencia" value="' . utf8_decode($ruta_referencia) . '"/><br>';
                  $form.='<div style="clear:both"><label for="txtResumen">Resumen:</label><textarea id="txtResumen" name="txtResumen">' . utf8_decode($resumen) . '</textarea></div>';
                  $form.='<div style="text-align:center;"><input type="button" id="butUpdate" value="Guardar">';
                  $form.='<input type="button" id="butCancelUpdate" value="Cancelar"></div>';
                  $form.='<input type="hidden" id="txtRutaImagen" name="txtRutaImagen" value="' . $ruta_imagen . '">';
                  $form.='<input type="hidden" id="txtRutaPdf" name="txtRutaPdf" value="' . $ruta_pdf . '">';
                  $form.='</form>';
            }
            $stmt->close();
            break;
      case 'Libros':
            $stmt = $oConni->prepare("SELECT * FROM LIBRO WHERE ID_LIBRO=?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id_libro, $titulo, $autor, $editorial, $resumen, $ruta_imagen, $ruta_pdf, $categoria);

            if ($stmt->fetch()) {
                  $stmt2 = $oConni2->prepare("SELECT * FROM LIBRO_CATEGORIA");
                  $stmt2->execute();
                  $stmt2->store_result();
                  $stmt2->bind_result($id_categoria, $nombre);

                  $select = '<label for="selCategorias">Categoría:</label><select id="selCategoriasEdicion" name="selCategoriasEdicion">';
                  while ($stmt2->fetch()) {
                        if ($id_categoria == $categoria) {
                              $select .= '<option value="' . $id_categoria . '" selected>' . $nombre . '</option>';
                        } else {
                              $select .= '<option value="' . $id_categoria . '">' . $nombre . '</option>';
                        }
                  }
                  $select .= '</select>';
                  $stmt2->close();
                  $form = '<form id="formularioEdicion" name="formularioEdicion" method="POST" enctype="multipart/form-data" accept-charset="UTF-8" data-id="' . $id . '">';
                  $form.=$select;
                  $form.='<div class="divInputFile"><span>Subir Imagen</span><input type="file" id="subirImagenEdicion" name="subirImagenEdicion" class="upload"/></div>';
                  $form.='<div class="divInputFile"><span>Subir PDF</span><input type="file" id="subirPDFEdicion" name="subirPDFEdicion" class="upload"/></div><br>';
                  $form.='<div id="divImgPreviaEdicion"><img class="imgPrevia" src="assets/images/' . utf8_decode($ruta_imagen) . '"/></div>';
                  $form.='<label for="txtTitulo">Titulo:</label><input type="text" id="txtTitulo" name="txtTitulo" value="' . utf8_decode($titulo) . '"/><br>';
                  $form.='<label for="txtAutor">Autor:</label><input type="text" id="txtAutor" name="txtAutor" value="' . utf8_decode($autor) . '"/><br>';
                  $form.='<label for="txtEditorial">Editorial:</label><input type="text" id="txtEditorial" name="txtEditorial" value="' . utf8_decode($editorial) . '"><br>';
                  $form.='<div style="clear:both"><label for="txtResumen">Resumen:</label><textarea id="txtResumen" name="txtResumen">' . utf8_decode($resumen) . '</textarea></div>';
                  $form.='<div style="text-align:center;"><input type="button" id="butUpdate" value="Guardar">';
                  $form.='<input type="button" id="butCancelUpdate" value="Cancelar"></div>';
                  $form.='<input type="hidden" id="txtRutaImagen" name="txtRutaImagen" value="' . $ruta_imagen . '">';
                  $form.='<input type="hidden" id="txtRutaPdf" name="txtRutaPdf" value="' . $ruta_pdf . '">';
                  $form.='</form>';
            }
            $stmt->close();
            break;
      case 'Vida cotidiana':
            $stmt = $oConni->prepare("SELECT * FROM VIDA_COTIDIANA WHERE ID_VIDA_COTIDIANA=?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id_vida_cotidiana, $titulo, $autor, $resumen, $ruta_imagen, $categoria);

            if ($stmt->fetch()) {
                  $stmt2 = $oConni2->prepare("SELECT * FROM VIDA_COTIDIANA_CATEGORIA");
                  $stmt2->execute();
                  $stmt2->store_result();
                  $stmt2->bind_result($id_categoria, $nombre);

                  $select = '<label for="selCategorias">Categoría:</label><select id="selCategoriasEdicion" name="selCategoriasEdicion">';
                  while ($stmt2->fetch()) {
                        if ($id_categoria == $categoria) {
                              $select .= '<option value="' . $id_categoria . '" selected>' . $nombre . '</option>';
                        } else {
                              $select .= '<option value="' . $id_categoria . '">' . $nombre . '</option>';
                        }
                  }
                  $select .= '</select><br>';
                  $stmt2->close();
                  $form = '<form id="formularioEdicion" name="formularioEdicion" method="POST" enctype="multipart/form-data" accept-charset="UTF-8" data-id="' . $id . '">';
                  $form.='<div class="divInputFile"><span>Subir Imagen</span><input type="file" id="subirImagenEdicion" name="subirImagenEdicion" class="upload"/></div>';
                  $form.='<div id="divImgPreviaEdicion"><img class="imgPrevia" src="assets/images/' . $ruta_imagen . '"/></div>';
                  $form.=$select;
                  $form.='<label for="txtTitulo">Titulo:</label><input type="text" id="txtTitulo" name="txtTitulo" value="' . utf8_decode($titulo) . '"/><br>';
                  $form.='<label for="txtAutor">Autor:</label><input type="text" id="txtAutor" name="txtAutor" value="' . utf8_decode($autor) . '"/><br>';
                  $form.='<div style="clear:both"><label for="txtResumen">Resumen:</label><textarea id="txtResumen" name="txtResumen">' . utf8_decode($resumen) . '</textarea></div>';
                  $form.='<div style="text-align:center;"><input type="button" id="butUpdate" value="Guardar">';
                  $form.='<input type="button" id="butCancelUpdate" value="Cancelar"></div>';
                  $form.='<input type="hidden" id="txtRutaImagen" name="txtRutaImagen" value="' . $ruta_imagen . '">';
                  $form.='</form>';
            }
            $stmt->close();
            break;
      case 'Preguntas':
            $stmt = $oConni->prepare("SELECT * FROM PREGUNTAS WHERE ID_PREGUNTAS=?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id_preguntas, $titulo, $respuesta, $categoria);

            if ($stmt->fetch()) {
                  $stmt2 = $oConni2->prepare("SELECT * FROM PREGUNTAS_CATEGORIA");
                  $stmt2->execute();
                  $stmt2->store_result();
                  $stmt2->bind_result($id_categoria, $nombre);

                  $select = '<label for="selCategorias">Categoría:</label><select id="selCategoriasEdicion" name="selCategoriasEdicion">';
                  while ($stmt2->fetch()) {
                        if ($id_categoria == $categoria) {
                              $select .= '<option value="' . $id_categoria . '" selected>' . $nombre . '</option>';
                        } else {
                              $select .= '<option value="' . $id_categoria . '">' . $nombre . '</option>';
                        }
                  }
                  $select .= '</select><br>';
                  $stmt2->close();
                  $form = '<div id="subject" data-id="' . $id . '">';
                  $form.='<form id="formularioEdicion" name="formularioEdicion" method="POST" enctype="multipart/form-data" accept-charset="UTF-8" data-id="' . $id . '">';
                  $form.=$select;
                  $form.='<label for="txtTitulo">Titulo:</label><input type="text" class="txtPreguntas" id="txtTitulo" name="txtTitulo" value="' . utf8_decode($titulo) . '"/><br>';
                  $form.='<label for="txtRespuesta">Respuesta:</label><textarea id="txtRespuesta" name="txtRespuesta">' . utf8_decode($respuesta) . '</textarea><br>';
                  $form.='<div style="text-align:center;"><input type="button" id="butUpdate" value="Guardar">';
                  $form.='<input type="button" id="butCancelUpdate" value="Cancelar"></div>';
                  $form.='</form></div>';
            }
            $stmt->close();
            break;
      case 'Bibliografia':
            $stmt = $oConni->prepare("SELECT * FROM BIBLIOGRAFIA WHERE ID_BIBLIOGRAFIA=?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id_bibliografia, $titulo, $autor, $resumen, $ruta_imagen, $ruta_pdf, $categoria);

            if ($stmt->fetch()) {
                  $stmt2 = $oConni2->prepare("SELECT * FROM BIBLIOGRAFIA_CATEGORIA");
                  $stmt2->execute();
                  $stmt2->store_result();
                  $stmt2->bind_result($id_categoria, $nombre);

                  $select = '<label for="selCategorias">Categoría:</label><select id="selCategoriasEdicion" name="selCategoriasEdicion">';
                  while ($stmt2->fetch()) {
                        if ($id_categoria == $categoria) {
                              $select .= '<option value="' . $id_categoria . '" selected>' . $nombre . '</option>';
                        } else {
                              $select .= '<option value="' . $id_categoria . '">' . $nombre . '</option>';
                        }
                  }
                  $select .= '</select>';
                  $stmt2->close();
                  $form = '<form id="formularioEdicion" name="formularioEdicion" method="POST" enctype="multipart/form-data" accept-charset="UTF-8" data-id="' . $id . '">';
                  $form.=$select;
                  $form.='<div class="divInputFile"><span>Subir Imagen</span><input type="file" id="subirImagenEdicion" name="subirImagenEdicion" class="upload"/></div>';
                  $form.='<div class="divInputFile"><span>Subir PDF</span><input type="file" id="subirPDFEdicion" name="subirPDFEdicion" class="upload"/></div><br>';
                  $form.='<div id="divImgPreviaEdicion"><img class="imgPrevia" src="assets/images/' . $ruta_imagen . '"/></div>';
                  $form.='<label for="txtTitulo">Titulo:</label><input type="text" id="txtTitulo" name="txtTitulo" value="' . utf8_decode($titulo) . '"/><br>';
                  $form.='<label for="txtAutor">Autor:</label><input type="text" id="txtAutor" name="txtAutor" value="' . utf8_decode($autor) . '"/><br>';
                  $form.='<div style="clear:both"><label for="txtResumen">Resumen:</label><textarea id="txtResumen" name="txtResumen">' . utf8_decode($resumen) . '</textarea></div>';

                  $form.='<div style="text-align:center;"><input type="button" id="butUpdate" value="Guardar">';
                  $form.='<input type="button" id="butCancelUpdate" value="Cancelar"></div>';
                  $form.='<input type="hidden" id="txtRutaImagen" name="txtRutaImagen" value="' . $ruta_imagen . '">';
                  $form.='<input type="hidden" id="txtRutaPdf" name="txtRutaPdf" value="' . $ruta_pdf . '">';
                  $form.='</form>';
            }
            $stmt->close();
            break;
      case 'Humor':
            $stmt = $oConni->prepare("SELECT * FROM HUMOR WHERE ID_HUMOR=?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id_humor, $titulo, $resumen, $ruta_imagen, $categoria);

            if ($stmt->fetch()) {
                  $stmt2 = $oConni2->prepare("SELECT * FROM HUMOR_CATEGORIA");
                  $stmt2->execute();
                  $stmt2->store_result();
                  $stmt2->bind_result($id_categoria, $nombre);

                  $select = '<label for="selCategorias">Categoría:</label><select id="selCategoriasEdicion" name="selCategoriasEdicion">';
                  while ($stmt2->fetch()) {
                        if ($id_categoria == $categoria) {
                              $select .= '<option value="' . $id_categoria . '" selected>' . $nombre . '</option>';
                        } else {
                              $select .= '<option value="' . $id_categoria . '">' . $nombre . '</option>';
                        }
                  }
                  $select .= '</select><br>';
                  $stmt2->close();
                  $form = '<form id="formularioEdicion" name="formularioEdicion" method="POST" enctype="multipart/form-data" accept-charset="UTF-8" data-id="' . $id . '">';
                  $form.=$select;
                  $form.='<div class="divInputFile"><span>Subir Imagen</span><input type="file" id="subirImagenEdicion" name="subirImagenEdicion" class="upload"/></div>';
                  $form.='<div id="divImgPreviaEdicion"><img class="imgPrevia" src="assets/images/' . $ruta_imagen . '"/></div>';
                  $form.='<label for="txtTitulo">Titulo:</label><input type="text" id="txtTitulo" name="txtTitulo" value="' . utf8_decode($titulo) . '"/><br>';
                  $form.='<div style="clear:both"><label for="txtResumen">Resumen:</label><textarea id="txtResumen" name="txtResumen">' . utf8_decode($resumen) . '</textarea></div>';
                  $form.='<div style="text-align:center;"><input type="button" id="butUpdate" value="Guardar">';
                  $form.='<input type="button" id="butCancelUpdate" value="Cancelar"></div>';
                  $form.='<input type="hidden" id="txtRutaImagen" name="txtRutaImagen" value="' . $ruta_imagen . '">';
                  $form.='</form>';
            }
            $stmt->close();
            break;
}
echo $form;

