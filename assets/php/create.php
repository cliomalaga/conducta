<?php

require 'conection.php';

$directorio = '/var/www/assets/';
$ext = array("bmp", "gif", "jpg", "jpeg", "png", "ico");

$category = $_POST['category'];
$categoria = $_POST["selCategorias"];

if (isset($_POST['txtTitulo'])) {
      $titulo = stripslashes(utf8_encode($_POST["txtTitulo"]));
}

if (isset($_POST['txtAutor'])) {
      $autor = stripslashes(utf8_encode($_POST["txtAutor"]));
}
if (isset($_POST['txtFecha'])) {
      $fecha = stripslashes(utf8_encode($_POST["txtFecha"]));
}
if (isset($_POST['txtResumen'])) {
      $resumen = stripslashes(utf8_encode($_POST["txtResumen"]));
}

if (isset($_POST['txtReferencia'])) {
      $referencia = stripslashes(utf8_encode($_POST["txtReferencia"]));
}
if (isset($_POST['txtRutaReferencia'])) {
      $ruta_referencia = stripslashes(utf8_encode($_POST["txtRutaReferencia"]));
}
if (isset($_POST['txtEditorial'])) {
      $editorial = stripslashes(utf8_encode($_POST["txtEditorial"]));
}
if (isset($_POST['txtRespuesta'])) {
      $respuesta = stripslashes(utf8_encode($_POST["txtRespuesta"]));
}
$namePdf = '';
$nameImagen = '';
$error = '';

//Subimos fotos o pdfs si se han subido archivos
if (isset($_FILES['subirImagen']['tmp_name'])) {
      if (is_uploaded_file($_FILES['subirImagen']['tmp_name'])) {
            if ($_FILES['subirImagen']['size'] < 5000000) {
                  $e = pathinfo($_FILES['subirImagen']['name']);
                  if (array_search($e['extension'], $ext)) { //Si es una de las extensiones que permitimos en el array
                        $nameImagen = $_FILES['subirImagen']['name'];
                        $tmpImagen = $_FILES['subirImagen']['tmp_name'];
                        $urlNueva = $directorio . 'images/' . $nameImagen;
                        copy($tmpImagen, $urlNueva);
                  } else {
                        $error = "Imagen no válida";
                  }
            } else {
                  $error = "Imagen demasiado grande";
            }
      }
}
if (isset($_FILES['subirPDF']['tmp_name'])) {
      if (is_uploaded_file($_FILES['subirPDF']['tmp_name'])) {
            if ($_FILES['subirPDF']['size'] < 50000000) {
                  if ($_FILES['subirPDF']['type'] == 'application/pdf' || $_FILES['subirPDF']['type'] == 'application/x-rar'|| $_FILES['subirPDF']['type'] == 'application/download') {
                        $namePdf = $_FILES['subirPDF']['name'];
                        $tmpPdf = $_FILES['subirPDF']['tmp_name'];
                        $urlNueva = $directorio . 'pdf/' . $namePdf;
                        copy($tmpPdf, $urlNueva);
                  } else {
                        $error = "PDF no válido";
                  }
            } else {
                  $error = "PDF demasiado grande";
            }
      }
}

switch ($category) {
      case 'Noticias':
            $stmt = $oConni->prepare('INSERT INTO NOTICIA (TITULO,RESUMEN,REFERENCIA,RUTA_REFERENCIA,RUTA_IMAGEN,RUTA_PDF,ID_CATEGORIA) VALUES (?,?,?,?,?,?,?)');
            $stmt->bind_param('ssssssi', $titulo, $resumen, $referencia, $ruta_referencia, $nameImagen, $namePdf, $categoria);
            break;
      case 'Articulos':
            if (!preg_match("^\d{1,2}\/\d{1,2}\/\d{4}$^", $fecha)) {
                  $fecha = "";
            }
            $stmt = $oConni->prepare("INSERT INTO ARTICULO (TITULO,AUTOR,FECHA_PUBLICACION,RESUMEN,RUTA_IMAGEN,RUTA_PDF,REFERENCIA,RUTA_REFERENCIA,ID_CATEGORIA) VALUES (?,?,?,?,?,?,?,?,?)");
            $stmt->bind_param('ssssssssi', $titulo, $autor, $fecha, $resumen, $nameImagen, $namePdf, $referencia, $ruta_referencia, $categoria);
            break;
      case 'Libros':
            $stmt = $oConni->prepare('INSERT INTO LIBRO (TITULO,AUTOR,EDITORIAL,RESUMEN,RUTA_IMAGEN,RUTA_PDF,ID_CATEGORIA) VALUES (?,?,?,?,?,?,?)');
            $stmt->bind_param('ssssssi', $titulo, $autor, $editorial, $resumen, $nameImagen, $namePdf, $categoria);
            break;
      case 'Vida cotidiana':
            $stmt = $oConni->prepare('INSERT INTO VIDA_COTIDIANA (TITULO,AUTOR,RESUMEN,RUTA_IMAGEN,ID_CATEGORIA) VALUES (?,?,?,?,?)');
            $stmt->bind_param('ssssi', $titulo, $autor, $resumen, $nameImagen, $categoria);
            break;
      case 'Preguntas':
            $stmt = $oConni->prepare('INSERT INTO PREGUNTAS (TITULO,RESPUESTA,ID_CATEGORIA) VALUES (?,?,?)');
            $stmt->bind_param('ssi', $titulo, $respuesta, $categoria);
            break;
      case 'Bibliografia':
            $stmt = $oConni->prepare('INSERT INTO BIBLIOGRAFIA (TITULO,AUTOR,RESUMEN,RUTA_IMAGEN,RUTA_PDF,ID_CATEGORIA) VALUES (?,?,?,?,?,?)');
            $stmt->bind_param('sssssi', $titulo, $autor, $resumen, $nameImagen, $namePdf, $categoria);
            break;
      case 'Humor':
            $stmt = $oConni->prepare('INSERT INTO HUMOR (TITULO,RESUMEN,RUTA_IMAGEN,ID_CATEGORIA) VALUES (?,?,?,?)');
            $stmt->bind_param('sssi', $titulo, $resumen, $nameImagen, $categoria);
            break;
}
$stmt->execute();
$stmt->close();

echo $error;


