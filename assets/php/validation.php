<?php
header('Content-Type: text/html; charset=utf-8');
require('conection.php');

if (isset($_POST['login']) or isset($_POST['password'])) {
	$clave= hash('sha512', $_POST['password']);
      $stmt = $oConni->prepare("SELECT ID,NOMBRE FROM USUARIOS WHERE NOMBRE= ? AND CLAVE = ?");
      $stmt->bind_param('ss', $_POST['login'], $clave);
      $stmt->execute();
      $stmt->store_result();
      $stmt->bind_result($id, $nombre);
      if ($stmt->fetch()) {
            setcookie("id_usuario", $id, time()+3600, "/");
            setcookie("nombre", $nombre, time()+3600,"/");
            header('location:../../admin.php');
	} else {
          header('location:../../login.html');
     }
      $stmt->close();
} else if (!isset($_COOKIE["id_usuario"])) {
    header('location:../../login.html');
}
