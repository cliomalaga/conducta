<?php

$stmt = $oConni->prepare("SELECT TITULO,AUTOR,FECHA_PUBLICACION,RESUMEN,RUTA_IMAGEN,RUTA_PDF,REFERENCIA,RUTA_REFERENCIA FROM ARTICULO WHERE ID_CATEGORIA IN (SELECT ID_CATEGORIA FROM ARTICULO_CATEGORIA) ORDER BY ID_ARTICULO DESC LIMIT 0,10");
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($titulo, $autor, $fecha, $resumen, $ruta_imagen, $ruta_pdf, $referencia, $ruta_referencia);

$monthNames = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

$vacio = true;
if ($stmt->num_rows > 0)
      $vacio = false;
echo '<div id="subject" data-id="Articulos">'; //Abrimos div SUBJECT
while ($stmt->fetch()) {
      $vacio = false;

      echo '<div class="elemento animate">'; //Abrimos div ARTICULOS
      echo '<div class="divTitulo">'; //Abrimos div TITULO

      if ($titulo != '') {
            echo '<div>' . utf8_decode($titulo) . '</div>';
      }

      if ($ruta_pdf != '') {
            echo '<a class="descargarPdf" href="../../assets/pdf/' . $ruta_pdf . '" download="' . $ruta_pdf . '"></a>';
      }
      echo '<img src="../../assets/img/mail.png" class="thumbs comentario"/>';
      echo '</div>'; //Cerramos div TITULO

      echo '<div class="divAutor">'; //Abrimos div AUTOR
      if ($autor != '') {
            echo '<strong>Autor:</strong> ' . utf8_decode($autor);
      }

      if ($fecha != '') {
            $month = (substr($fecha, 3, 2) - 1);
            $fecha = substr($fecha, 0, 2) . ' de ' . $monthNames[$month] . ' de ' . substr($fecha, 6, 4);
            echo '<br>Publicado el ' . $fecha;
      }

      if ($referencia != '' && $ruta_referencia != '') { //Div REFERENCIA
            echo ' - (<a href="http://' . $ruta_referencia . '" target="_blank">' . utf8_decode($referencia) . '</a>)';
      }
      echo '</div><br>'; //Cerramos div AUTOR

      if ($ruta_imagen != '') {//Div IMAGEN
            echo '<div class="divImagen"><img src="../../assets/images/' . $ruta_imagen . '" class="img"></div>';
      }

      if ($resumen != '') {
            echo '<div class="divResumen">' . nl2br(utf8_decode($resumen)) . '</div>'; //Div RESUMEN
      }

      if ($ruta_pdf != '') {
            echo '<div class="divLeer"><a class="leerMas" name="../../assets/pdf/' . $ruta_pdf . '" title="Leer más sobre ' . $ruta_pdf . '">Leer más...</a></div>';
      }
      echo '<br></div>'; //Cerramos div ARTICULOS
}
echo '</div>';
$stmt->close();
