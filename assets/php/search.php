<?php

require 'conection.php';

$cadena = "%" . utf8_encode($_POST["cadena"]) . "%";

$stmt = $oConni->prepare("SELECT * FROM NOTICIA WHERE TITULO LIKE ? OR RESUMEN LIKE ?");
$stmt->bind_param('ss', $cadena, $cadena);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($id_noticia, $titulo, $resumen, $referencia, $ruta_referencia, $ruta_imagen, $ruta_pdf, $categoria);

$html = "";
while ($stmt->fetch()) {
      $html .='<div class="elemento animate">'; //Abrimos div NOTICIAS
      $html .='<div class="divTitulo">'; //Abrimos div TITULO

      if ($titulo != '') {
            $html.='<div>' . mark(utf8_decode($titulo)) . '</div>';
      }

      if ($ruta_pdf != '') {
            $html.='<a class="descargarPdf" href="assets/pdf/' . $ruta_pdf . '" download="' . $ruta_pdf . '"></a>';
      }
      $html.='<img src="assets/img/mail.png" class="thumbs comentario"/>';
      $html.='</div>'; //Cerramos div TITULO

      if ($referencia != '' && $ruta_referencia != '') {//div REFERENCIA
            $html .='<div class="divReferencia"><a href="http://' . $ruta_referencia . '" target="_blank">' . mark(utf8_decode($referencia)) . '</a></div>';
      }
      $html.='<br>';

      if ($ruta_imagen != '') {
            $html .= '<div class="divImagen"><img src="assets/images/' . $ruta_imagen . '" class="img"></div>';
      }

      if ($resumen != '') {
            $html .='<div class="divResumen">' . nl2br(mark(utf8_decode($resumen))) . '</div>'; //Div RESUMEN
      }

      if ($ruta_pdf != '') {
            $html.='<div class="divLeer"><a class="leerMas" name="assets/pdf/' . $ruta_pdf . '" title="Leer más sobre ' . $ruta_pdf . '">Leer más...</a></div>';
      }
      $html.='<br></div>'; //Cerramos div NOTICIAS
}
$stmt->close();
//-----------------------------------------------------------------------------------------------------------------------------------------------------------

$stmt = $oConni->prepare("SELECT * FROM ARTICULO WHERE TITULO LIKE ? OR AUTOR LIKE ? OR RESUMEN LIKE ?");
$stmt->bind_param('sss', $cadena, $cadena, $cadena);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($id_articulo, $titulo, $autor, $fecha, $resumen, $ruta_imagen, $ruta_pdf, $referencia, $ruta_referencia, $categoria);

while ($stmt->fetch()) {
      $html .='<div class="elemento animate">'; //Abrimos div ARTICULOS
      $html .='<div class="divTitulo">'; //Abrimos div TITULO

      if ($titulo != '') {
            $html.='<div>' . mark(utf8_decode($titulo)) . '</div>';
      }

      if ($ruta_pdf != '') {
            $html.='<a class="descargarPdf" href="assets/pdf/' . $ruta_pdf . '" download="' . $ruta_pdf . '"></a>';
      }
      $html.='<img src="assets/img/mail.png" class="thumbs comentario"/>';
      $html.='</div>'; //Cerramos div TITULO

      $html .='<div class="divAutor">'; //Abrimos div AUTOR
      if ($autor != '') {
            $html.='<strong>Autor:</strong> ' . mark(utf8_decode($autor));
      }

      if ($referencia != '' && $ruta_referencia != '') { //Div REFERENCIA
            $html .=' - (Extraido de <a href="http://' . $ruta_referencia . '" target="_blank">' . mark(utf8_decode($referencia)) . '</a>)';
      }
      $html.='</div><br>'; //Cerramos div AUTOR

      if ($ruta_imagen != '') {//Div IMAGEN
            $html .= '<div class="divImagen"><img src="assets/images/' . $ruta_imagen . '" class="img"></div>';
      }

      if ($resumen != '') {
            $html .='<div class="divResumen">' . nl2br(mark(utf8_decode($resumen))) . '</div>'; //Div RESUMEN
      }

      if ($ruta_pdf != '') {
            $html.='<div class="divLeer"><a class="leerMas" name="assets/pdf/' . $ruta_pdf . '" title="Leer más sobre ' . $ruta_pdf . '">Leer más...</a></div>';
      }
      $html.='<br>';
      $html.='</div>'; //Cerramos div ARTICULOS
}
$stmt->close();
//-----------------------------------------------------------------------------------------------------------------------------------------------------------
$stmt = $oConni->prepare("SELECT * FROM BIBLIOGRAFIA WHERE TITULO LIKE ? OR AUTOR LIKE ? OR RESUMEN LIKE ?");
$stmt->bind_param('sss', $cadena, $cadena, $cadena);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($id_bibliografia, $titulo, $autor, $resumen, $ruta_imagen, $ruta_pdf, $categoria);

while ($stmt->fetch()) {
      $html .='<div class="elemento animate">'; //Abrimos div ARTICULOS
      $html .='<div class="divTitulo">'; //Abrimos div TITULO

      if ($titulo != '') {
            $html.='<div>' . mark(utf8_decode($titulo)) . '</div>';
      }

      if ($ruta_pdf != '') {
            $html.='<a class="descargarPdf" href="pdf/' . $ruta_pdf . '" download="' . $ruta_pdf . '"></a>';
      }
      $html.='<img src="assets/img/mail.png" class="thumbs comentario"/>';
      $html.='</div>'; //Cerramos div TITULO

      if ($autor != '') {
            $html.='<div class="divAutor"><strong>Autor:</strong> ' . mark(utf8_decode($autor)) . '</div>'; //Div AUTOR
      }
      $html.='<br>';

      if ($ruta_imagen != '') {
            $html .= '<div class="divImagen"><img src="assets/images/' . $ruta_imagen . '" class="img"></div>'; //Div IMAGEN
      }

      if ($resumen != '') {
            $html .='<div class="divResumen">' . nl2br(mark(utf8_decode($resumen))) . '</div>'; //Div RESUMEN
      }

      if ($ruta_pdf != '') {
            $html.='<div class="divLeer"><a class="leerMas" name="pdf/' . $ruta_pdf . '" title="Leer más sobre ' . $ruta_pdf . '">Leer más...</a></div>';
      }

      $html.='<br></div>'; //Cerramos div ARTICULOS
}
$stmt->close();
//-----------------------------------------------------------------------------------------------------------------------------------------------------------
$stmt = $oConni->prepare("SELECT * FROM HUMOR WHERE TITULO LIKE ? OR RESUMEN LIKE ?");
$stmt->bind_param('ss', $cadena, $cadena);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($id_humor, $titulo, $resumen, $ruta_imagen, $categoria);

while ($stmt->fetch()) {
      $html .='<div class="elemento animate">'; //Abrimos div  HUMOR
      if ($titulo != '') {
            $html.='<div class="divTitulo"><div>' . mark(utf8_decode($titulo)) . '</div></div>'; //Div TITULO
      }
      if ($ruta_imagen != '') {
            $html .= '<div class="divImagen"><img src="assets/images/' . $ruta_imagen . '" class="img"></div>'; //Div IMAGEN
      }
      if ($resumen != '') {
            $html .='<div class="divResumen">' . nl2br(mark(utf8_decode($resumen))) . '</div>'; //Div RESUMEN
      }
      $html.='</div>'; //Cerramos div HUMOR
}
$stmt->close();
//-----------------------------------------------------------------------------------------------------------------------------------------------------------
$stmt = $oConni->prepare("SELECT * FROM LIBRO WHERE TITULO LIKE ? OR AUTOR LIKE ? OR RESUMEN LIKE ?");
$stmt->bind_param('sss', $cadena, $cadena, $cadena);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($id_libro, $titulo, $autor, $editorial, $resumen, $ruta_imagen, $ruta_pdf, $categoria);

while ($stmt->fetch()) {
      $html .='<div class="elemento animate">'; //Abrimos div LIBROS
      $html .='<div class="divTitulo">'; //Abrimos div TITULO

      if ($titulo != '') {
            $html.='<div>' . mark(utf8_decode($titulo)) . '</div>';
      }

      if ($ruta_pdf != '') {
            $html.='<a class="descargarPdf" href="assets/pdf/' . $ruta_pdf . '" download="' . $ruta_pdf . '"></a>';
      }
      $html.='<img src="assets/img/mail.png" class="thumbs comentario"/>';
      $html.='</div>'; //Cerramos div TITULO

      $html .='<div class="divAutor">'; //Abrimos div AUTOR/EDITORIAL
      if ($autor != '') {
            $html.='<strong>Autor:</strong> ' . mark(utf8_decode($autor));
      }
      if ($editorial != '') {
            $html.=' - Editorial: ' . $editorial;
      }
      $html.='</div><br>'; //Cerramos div AUTOR/EDITORIAL

      if ($ruta_imagen != '') {
            $html .= '<div class="divImagen"><img src="assets/images/' . $ruta_imagen . '" class="img"></div>';
      }

      if ($resumen != '') {
            $html .='<div class="divResumen">' . nl2br(mark(utf8_decode($resumen))) . '</div>'; //Div RESUMEN
      }

      if ($ruta_pdf != '') {
            $html.='<div class="divLeer"><a class="leerMas" name="assets/pdf/' . $ruta_pdf . '" title="Leer más sobre ' . $ruta_pdf . '">Leer más...</a></div>';
      }
      $html.='<br></div>'; //Cerramos div LIBROS
}
$stmt->close();
//-----------------------------------------------------------------------------------------------------------------------------------------------------------
$stmt = $oConni->prepare("SELECT * FROM PREGUNTAS WHERE TITULO LIKE ? OR RESPUESTA LIKE ?");
$stmt->bind_param('ss', $cadena, $cadena);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($id_preguntas, $titulo, $respuesta, $categoria);

while ($stmt->fetch()) {
      $html.='<div  id="pregunta' . $id_preguntas . '" class="elemento animate">';
      if ($titulo != '') {
            $html .= '<p class="titlePregunta"><img id="arrow' . $id_preguntas . '" class="arrows" src="assets/img/mas.png"/> ' . mark(utf8_decode($titulo)) . '</p>';
      }
      if ($respuesta != '') {
            $html .= '<div class="divRespuesta" style="display:none;"><br>' . nl2br(mark(utf8_decode($respuesta))) . '</div>';
      }
      $html.='</div>';
}
$stmt->close();
//-----------------------------------------------------------------------------------------------------------------------------------------------------------
$stmt = $oConni->prepare("SELECT * FROM VIDA_COTIDIANA WHERE TITULO LIKE ? OR AUTOR LIKE ? OR RESUMEN LIKE ?");
$stmt->bind_param('sss', $cadena, $cadena, $cadena);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($id_vida_cotidiana, $titulo, $autor, $resumen, $ruta_imagen, $categoria);

while ($stmt->fetch()) {
      $html .='<div class="elemento animate">'; //Abrimos div ARTICULOS
      $html .='<div class="divTitulo">'; //Abrimos div TITULO

      if ($titulo != '') {
            $html.='<div>' . mark(utf8_decode($titulo)) . '</div>';
      }
      $html.='<img src="assets/img/mail.png" class="thumbs comentario"/>';
      $html.='</div>'; //Cerramos div TITULO

      if ($autor != '') {
            $html.='<div class="divAutor"><strong>Autor:</strong> ' . mark(utf8_decode($autor)) . '</div>';
      }

      $html.='<br>';

      if ($ruta_imagen != '') {
            $html .= '<div class="divImagen"><img src="assets/images/' . $ruta_imagen . '" class="img"></div>';
      }

      if ($resumen != '') {
            $html .='<div class="divResumen">' . nl2br(mark(utf8_decode($resumen))) . '</div>'; //Div RESUMEN
      }
      $html.='<br></div>';
}
$stmt->close();

//-----------------------------------------------------------------------------------------------------------------------------------------------------------

function mark($_cadena) {
      $_cadena = str_replace($_POST['cadena'], "<mark>" . $_POST['cadena'] . "</mark>", $_cadena);
      $_cadena = str_replace(strtolower($_POST['cadena']), "<mark>" . strtolower($_POST['cadena']) . "</mark>", $_cadena);
      $_cadena = str_replace(strtoupper($_POST['cadena']), "<mark>" . strtoupper($_POST['cadena']) . "</mark>", $_cadena);
      $_cadena = str_replace(ucfirst($_POST['cadena']), "<mark>" . ucfirst($_POST['cadena']) . "</mark>", $_cadena);
      return $_cadena;
}

if ($html == '')
      $html = '<div class="elemento animate"><div class="divTitulo"><strong>No hay resultados</strong></div></div>';
echo $html;
