<?php

header('Content-Type: text/html; charset=utf-8');
$subcategory = $_POST['subcategory'];

if ($subcategory == "Quienes somos") {
      require '../assets/php/conection.php';
      $stmt = $oConni->prepare("SELECT NOMBRE,EMAIL,FOTO FROM PROFESORES ORDER BY NOMBRE ASC");
      $stmt->execute();
      $stmt->store_result();
      $stmt->bind_result($nombre, $email, $foto);
      $html = '<div id="divProfesores">'; //Abrimos div PROFESORES
      while ($stmt->fetch()) {
            $html.= '<div class="divProfesor elemento animate">'; //Abrimos div PROFESOR
            $html.='<div class="divImgProfesor">';
            if ($foto != '') {
                  $html .='<img data-id="' . utf8_encode($nombre) . '-' . utf8_encode($email) . '" class="fotoProfesor" src="data:image/*;base64,' . base64_encode($foto) . '" />';
            } else {
                  $html .='<img data-id="' . utf8_encode($nombre) . '-' . utf8_encode($email) . '" class="fotoProfesor" src="assets/img/sinavatar.jpg" />';
            }
            $html.='</div>';
            $html.='<div class="divDatosProfesor"><h4>' . utf8_encode($nombre) . '</h4>';
            $html.='<h4><a href="mailto:' . utf8_encode($email) . '">' . utf8_encode($email) . '</a></h4></div>';
            $html.='</div>'; //Cerramos div PROFESOR
      }
      $html.='</div>'; //Cerramos div PROFESORES
      echo json_encode(array("html" => $html, "input" => ''));
} else if ($subcategory == "Editorial") {
      $html = '<div class="divEditorial animate"><p>Bienvenido a la Página Web de Contextos.</p>
<p>Contextos es una iniciativa sin ánimo de lucro, y desvinculada de cualquier empresa, entidad u organismo, público o privado. Las colaboraciones prestadas a la Página Web lo son, únicamente, a título altruista. Los Colaboradores de Contextos carecen de remuneración por la realización de dichas colaboraciones.</p>
<p>A estos efectos, Contextos se entiende constituye una denominación para designar conjuntamente a los Colaboradores de la Página Web de Contextos, cuyos nombres se indican en la misma.</p>
<p>La Página Web tiene por objeto el intercambio de información y la mutua colaboración entre profesionales de la Psicología, así como la defusión de esos contenidos hacia el público en general. La utilización de esta publicación digital y sus contenidos están sujetos a las condiciones de uso que expresa en Autorizaciones, por lo que rogamos al Usuario que proceda a su lectura atentamente antes de hacer uso del resto de contenidos de la Página Web. </p></div>';
      echo json_encode(array("html" => $html, "input" => ''));
} else if ($subcategory == "Jornadas") {
      $html = '<div class="divJornadas animate">"Enlace a I Jornadas Contextos (Madrid, 11 de Mayo de 2002)</div>
<div class="divJornadas animate">Enlace a II Jornadas Contextos (Madrid, 28 de Febrero de 2004)</div>
<div class="divJornadas animate">Enlace a III Jornadas Contextos  (Málaga, 20-21 de marzo de 2009)</div>';
      echo json_encode(array("html" => $html, "input" => ''));
} else if ($subcategory == "Compras") {
      $html = '<div class="divCompras animate">Puedes ayudarnos a mantener la web de Conducta.org  al realizar alguna compra de los libros que ofrecemos, DVD de jornadas realizadas por Contextos, y otros materiales del grupo que ofrecemos en las páginas.</div>';
      echo json_encode(array("html" => $html, "input" => ''));
} else if ($subcategory == "Colaboraciones") {
      $html = '<div class="divColaboraciones animate"><p>Puedes colaborar con nosotros enviando noticias, congresos, jornadas, novedades editoriales, etc., sobre los temas tratados en Conducta.org. También puedes colaborar enviando artículos y textos, que una vez revisados por dos colaboradores independientes, aceptaremos su publicación en la web. </p><p>No somos una revista científica al uso, ni tenemos JCR, pero la difusión de la web alcanza a miles de personas cada día de todo el mundo que hablan y escriben en español.
</p><p>Al menos, colabora con nosotros, dándonos un feedback sobre lo que te parece la web, los artículos, materiales, etc., así como errores, mal funcionamiento, o “despistes” en algunos de los contenidos.</p></div>';
      echo json_encode(array("html" => $html, "input" => ''));
} else if ($subcategory == "Autorizaciones") {
      $html = '<div class="divAutorizaciones animate"><p><strong>Las presentes Condiciones de Uso se rigen por la ley española. Para cualquier controversia que pudiera derivarse de la aplicación o interpretación de las mismas, los Colaboradores de Contextos y el Usuario, con renuncia expresa a cualquier otro fuero que pudiera corresponderles, se someten al de los Juzgados y Tribunales de la ciudad de Madrid (España).</strong></p>
<br><p><strong>1. Derechos de Propiedad Intelectual.</strong></p>
<p>El software, la información, textos, imágenes, sonidos, animaciones y el resto de contenidos incluidos en la Página Web son propiedad exclusiva deI/los Colaborador/es de Contextos que en cada caso se indique/n, correspondiendo a éstos todos los derechos de propiedad intelectual sobre los mismos.
Los contenidos, artículos, textos, libros, material informático, videográfico o infográfico elaborado por los miembros del Grupo Contextos están sujetos a licencia Creative Commons con Reconocimiento (Attribution) y No Comercial (Non commercial). Lo que supone que cualquier otra difusión o reproducción de esos contenidos necesitará reconocer la autoría del autor o autores concretos, así como la web desde la que se extrajo esa información. Así mismo, cualquier otra explotación educativa o divulgación no tendrá nunca un propósito comercial.</p>
<p>La Página Web puede contener software y/o contenidos propiedad de terceros ajenos a Contextos, por lo que el Usuario queda igualmente obligado al respeto de los derechos que correspondan a dichos terceros.</p>
<p>En el supuesto de que se autorice alguno de dichos actos, el Usuario sólo quedará autorizado a su realización en los términos y con el alcance que expresamente se indiquen en la misma. Salvo que otra cosa se indique expresamente en la autorización, ésta se entenderá concedida únicamente para fines informativos, educativos, o investigación, y no comerciales, y el Usuario quedará obligado a incluir referencia del propietario en cualquier uso que hiciera del software y/o contenidos.</p>
<br><p><strong>2. Derechos de Propiedad Industrial.</strong></p>
<p>La Página Web puede contener marcas/denominaciones de Contextos o referencias a marcas/denominaciones de terceros, por lo que queda prohibido su uso sin el previo consentimiento expreso y escrito de Contextos y/o deI/los tercero/s propietario/s, según los casos.</p>
<p>En el supuesto de que se autorice uso alguno de dichas marcas/denominaciones, el Usuario sólo quedará autorizado en los términos y con el alcance que expresamente se indiquen en la misma. Salvo que otra cosa se indique expresamente en la autorización, ésta se entenderá concedida únicamente para fines informativos y no comerciales, y el Usuario quedará obligado a incluir referencia del propietario en cualquier uso que hiciera de las citadas marcas/denominaciones.</p>
<br><p><strong>3. Responsabilidad.</strong></p>
<p>Con las limitaciones que dispongan las leyes que resulten aplicables en cada momento, Contextos no asume responsabilidad alguna por la publicación de la Página Web, siendo la información y contenidos que aparecen en ella únicamente de carácter informativo y divulgativo, sin constituir consejo o asesoramiento profesional alguno.</p>
<p>No se puede garantizar la disponibilidad ni continuidad de la Página Web, ni la exactitud, veracidad y actualización de sus contenidos, pese a los esfuerzos para velar por ello. Asimismo, la Página Web podrá cambiarse o actualizarse sin previo aviso.</p>
<p>El Usuario es consciente y acepta voluntariamente que el acceso a la Página Web, su uso, y el de su software y contenidos, tiene lugar bajo su única y exclusiva responsabilidad. No se asume ninguna responsabilidad derivada del uso incorrecto, inapropiado o ilícito del software y contenidos de la Página Web, siendo el Usuario el único responsable de los posibles daños causados por la descarga, apertura o uso de cualquier material del mismo, inclusive, aquellos daños que deriven de una eventual y no querida transmisión de "virus" informáticos.</p>
<p>Enlaces con otros sitios web y publicidad</p>
<p>Esta página Web puede incluir enlaces de otros sitios de Internet ("links"). Ello no implica en ningún caso la existencia de relaciones entre Contextos y los propietarios de los "links", ni la aprobación y aceptación, por parte de Contextos, de los mismos, ni de sus contenidos y servicios. En consecuencia, Contextos no asume responsabilidad alguna al respecto, ni de la corrección, veracidad y actualización de los contenidos aparecidos en los "links".</p>
<p>En su caso, la inserción en la Página Web de publicidad o información de cualesquiera empresa o marca comercial responde a la contratación de espacio publicitario, como forma de financiación parcial de dicha página, o bien a acuerdos o convenios de colaboración suscritos con idéntica finalidad.</p>
<br><p><strong>4. Confidencialidad.</strong></p>
<p>Sin perjuicio de lo dispuesto en las presentes Condiciones de Uso respecto a los derechos de propiedad que correspondan a terceros ajenos a Contextos, toda la información que sea enviada a la Página Web, ya sea a través de la misma o de sus Colaboradores, podrá ser utilizada, reproducida, transformada o difundida por Contextos a voluntad, salvo indicación expresa en contrario y para cada caso por parte del Usuario .</p>
<br><p><strong>5. Protección de Datos Personales</strong></p>
<p>En el supuesto de que la información suministrada por el Usuario y el uso que de la misma pueda hacer Contextos en cada momento queden bajo el ámbito de aplicación de la ley vigente en materia de protección de Datos Personales, Contextos se obliga a dar cumplimiento a dicha ley y a respetar todos los derechos que correspondan al Usuario afectado.</p>
<p>En particular, Contextos podrá conservar dicha información para fines estadísticos e históricos, y no cederá esas informaciones a terceros ni para uso publicitario. El usuario o consultante podrá expresar su deseo de dar de baja un texto o información aparecida en la web, expresando a Contextos esa petición de rectificación, salvo indicación expresa en contrario por parte del mismo. El Usuario afectado podrá ejercitar en todo momento sus derechos, en particular, los de acceso, rectificación y cancelación de los Datos Personales almacenados.</p>
<p>La página Conducta.org no utiliza cookies, ni scripts ni programas para el seguimiento de los usuarios. Solo utiliza las estadísticas generales de uso por países y días.</p>
<br><p><strong>6. Contacto con Contextos</strong></p>
<p>Para cualquier petición, consulta, solicitud de aclaración o de información adicional en relación con las presentes Condiciones de Uso, el Usuario debe dirigirse a la siguiente dirección de correo electrónico:</p>
<br><br>
<div id="divCc"><img src="assets/img/cc.png"/ style="margin:0 auto;"><p><strong>Coordinador académico: </strong>  luis@conducta.org</p></div></div>';
      echo json_encode(array("html" => $html, "input" => ''));
} else if ($subcategory == "Lista AC") {
      $html = '<div class="divAC animate">
            <p>Lista analisisconducta@uma.es </p>

 <p><strong><a href="http://sympa.sci.uma.es/uma/info/analisisconducta" target="_blank">http://sympa.sci.uma.es/uma/info/analisisconducta </a> </strong></p>

 <p>En esta lista se discuten temas especializados sobre Psicología de la Conducta, conductismo, análisis funcional de la conducta, aprendizaje, modificación de conducta, metodología en el estudio de la conducta, teorías conductistas, etc., </p>

 <p>Hay más de 1200 personas inscritas de todos los países hispanoamericanos. Entre ellos se envían informaciones, novedades, preguntas, opiniones, etc. </p>

 <p>Puedes inscribirte (es gratuito) y tener acceso a los ficheros de preguntas e informaciones de los últimos años, y enviar/recibir cualquier tipo de mensaje que se produzca en esta lista especializada. </p>

 <p>Para inscribirse entrar en la web del servicio de listas de discusión, darse de alta con un correo y clave propias, y luego se puede configurar la forma de recibir los mensajes conforme se producen, o agrupados por resúmenes semanales o mensuales. </p></div>';
      echo json_encode(array("html" => $html, "input" => ''));
}

