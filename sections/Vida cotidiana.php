<?php require '../assets/php/conection.php'; ?>
<?php

header('Content-Type: text/html; charset=utf-8');

$category = $_POST['category'];
$subcategory = $_POST['subcategory'];
$contador = $_POST['contador'];
$cantidad = $_POST['cantidad'];
$stmt = $oConni->prepare("SELECT TITULO,AUTOR, RUTA_IMAGEN, RESUMEN FROM VIDA_COTIDIANA WHERE ID_CATEGORIA = (SELECT ID_CATEGORIA FROM VIDA_COTIDIANA_CATEGORIA WHERE NOMBRE = ?) ORDER BY ID_VIDA_COTIDIANA DESC LIMIT ?,?");
$stmt->bind_param("sii", $subcategory, $contador, $cantidad);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($titulo, $autor, $ruta_imagen, $resumen);

$vacio=true;
if($stmt->num_rows>0)
    $vacio=false;
$html = '<div id="subject" data-id="' . $category . '-' . $subcategory . '">'; //Abrimos div SUBJECT
while ($stmt->fetch()) {
      $html .='<div class="elemento animate">'; //Abrimos div ARTICULOS
      $html .='<div class="divTitulo">'; //Abrimos div TITULO

      if ($titulo != '') {
            $html.= '<div>'.utf8_decode($titulo).'</div>' ;
      }
      $html.='<img src="assets/img/mail.png" class="thumbs comentario"/>';
      $html.='</div>'; //Cerramos div TITULO

      if ($autor != '') {
            $html.='<div class="divAutor"><strong>Autor:</strong> ' . utf8_decode($autor) . '</div>';
      }

           $html.='<br>';

      if ($ruta_imagen != '') {
            $html .= '<div class="divImagen"><img src="assets/images/' . $ruta_imagen . '" class="img"></div>';
      }

      if ($resumen != '') {
            $html .='<div class="divResumen">' .nl2br( utf8_decode($resumen)) . '</div>'; //Div RESUMEN
      }
    $html.='<br></div>';
}
$html.='</div>';
$stmt->close();

$stmt = $oConni->prepare("SELECT * FROM VIDA_COTIDIANA WHERE ID_CATEGORIA IN (SELECT ID_CATEGORIA FROM VIDA_COTIDIANA_CATEGORIA WHERE NOMBRE LIKE ?)");
$stmt->bind_param("s", $subcategory);
$stmt->execute();
$stmt->store_result();
$n = $stmt->num_rows;

$orden="mostrarMas('".$category."','".$subcategory."',".($contador+$cantidad).")";
if($n>($contador+$cantidad)){
    $input='<input type="button" id="mas" name="mas" class="animate" onclick="'.$orden.'"/ value="Mostrar mas resultados...">';
}else{
    $input='';
}

if ($vacio) {
      $html = '<div class="elemento animate"><div class="divTitulo"><strong>No hay resultados</strong></div></div>';
    $input='';
}
$stmt->close();

echo json_encode(array("html" => $html,"input"=>$input));
