<?php require '../assets/php/conection.php'; ?>
<?php

header('Content-Type: text/html; charset=utf-8');

$category = $_POST['category'];
$subcategory = $_POST['subcategory'];
$contador = $_POST['contador'];
$cantidad = $_POST['cantidad'];
$stmt = $oConni->prepare("SELECT TITULO,AUTOR,FECHA_PUBLICACION,RESUMEN,RUTA_IMAGEN,RUTA_PDF,REFERENCIA,RUTA_REFERENCIA FROM ARTICULO WHERE ID_CATEGORIA IN (SELECT ID_CATEGORIA FROM ARTICULO_CATEGORIA WHERE NOMBRE LIKE ?) ORDER BY ID_ARTICULO DESC LIMIT ?,?");
$stmt->bind_param("sii", $subcategory, $contador, $cantidad);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($titulo, $autor, $fecha, $resumen, $ruta_imagen, $ruta_pdf, $referencia, $ruta_referencia);

$monthNames = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

$vacio = true;
$html = '<div id="subject" data-id="' . $category . '-' . $subcategory . '">'; //Abrimos div SUBJECT

while ($stmt->fetch()) {
      $vacio = false;

      $html .='<div class="elemento animate">'; //Abrimos div ARTICULOS
      $html .='<div class="divTitulo">'; //Abrimos div TITULO

      if ($titulo != '') {
            $html.= '<div>' . utf8_decode($titulo) . '</div>';
      }

      if ($ruta_pdf != '') {
            $html.='<a class="descargarPdf" href="assets/pdf/' . $ruta_pdf . '" download="' . $ruta_pdf . '"></a>';
      }
      $html.='<img src="assets/img/mail.png" class="thumbs comentario"/>';
      $html.='</div>'; //Cerramos div TITULO

      $html .='<div class="divAutor">'; //Abrimos div AUTOR
      if ($autor != '') {
            $html.='<strong>Autor:</strong> ' . utf8_decode($autor);
      }

      if ($fecha != '') {
            $month = (substr($fecha, 3, 2) - 1);
            $fecha = substr($fecha, 0, 2) . ' de ' . $monthNames[$month] . ' de ' . substr($fecha, 6, 4);
            $html.='<br>Publicado el ' . $fecha;
      }

      if ($referencia != '' && $ruta_referencia != '') { //Div REFERENCIA
            $html .=' - (<a href="http://' . $ruta_referencia . '" target="_blank">' . utf8_decode($referencia) . '</a>)';
      }
      $html.='</div><br>'; //Cerramos div AUTOR

      if ($ruta_imagen != '') {//Div IMAGEN
            $html .= '<div class="divImagen"><img src="assets/images/' . $ruta_imagen . '" class="img"></div>';
      }

      if ($resumen != '') {
            $html .='<div class="divResumen">' . nl2br(utf8_decode($resumen)) . '</div>'; //Div RESUMEN
      }

      if ($ruta_pdf != '') {
            $html.='<div class="divLeer"><a class="leerMas" name="assets/pdf/' . $ruta_pdf . '" title="Leer más sobre ' . $ruta_pdf . '">Leer más...</a></div>';
      }
      $html.='<br></div>'; //Cerramos div ARTICULOS
}
$html.='</div>';
$stmt->close();

$stmt = $oConni->prepare("SELECT * FROM ARTICULO WHERE ID_CATEGORIA IN (SELECT ID_CATEGORIA FROM ARTICULO_CATEGORIA WHERE NOMBRE LIKE ?)");
$stmt->bind_param("s", $subcategory);
$stmt->execute();
$stmt->store_result();
$n = $stmt->num_rows;

$orden = "mostrarMas('" . $category . "','" . $subcategory . "'," . ($contador + $cantidad) . ")";
if ($n > ($contador + $cantidad)) {
      $input = '<input type="button" id="mas" name="mas" class="animate" onclick="' . $orden . '" value="Mostrar mas resultados..."/>';
} else {
      $input = '';
}

if ($vacio) {
      $html = '<div class="elemento animate"><div class="divTitulo"><strong>No hay resultados</strong></div></div>';
      $input = '';
}
$stmt->close();

echo json_encode(array("html" => $html, "input" => $input));
