<?php require '../assets/php/conection.php'; ?>
<?php

header('Content-Type: text/html; charset=utf-8');

$category = $_POST['category'];
$subcategory = $_POST['subcategory'];
$stmt = $oConni->prepare("SELECT ID_PREGUNTAS,TITULO,RESPUESTA FROM PREGUNTAS WHERE ID_CATEGORIA = (SELECT ID_CATEGORIA FROM PREGUNTAS_CATEGORIA WHERE NOMBRE = ?) ORDER BY ID_PREGUNTAS DESC");
$stmt->bind_param("s", $subcategory);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($id_preguntas, $titulo, $respuesta);

$vacio=true;
if($stmt->num_rows>0)
    $vacio=false;
$html = '<div id="subject" data-id="' . $category . '-' . $subcategory . '">'; //Abrimos div SUBJECT
while ($stmt->fetch()) {
    	$html.='<div  id="pregunta' . $id_preguntas . '" class="elemento animate">';
      if ($titulo != '') {
            $html .= '<p class="titlePregunta"><img id="arrow' . $id_preguntas . '" class="arrows" src="assets/img/mas.png"/> ' . utf8_decode($titulo) . '</p>';
      }
      if ($respuesta != '') {
            $html .= '<div class="divRespuesta" style="display:none;"><br>' . nl2br(utf8_decode($respuesta)) . '</div>';
      }
      $html.='</div>';
}
$html.='</div>';
if ($vacio) {
      $html = '<div class="elemento animate"><div class="divTitulo"><strong>No hay resultados</strong></div></div>';
}


$stmt->close();
 echo json_encode(array("html" => $html,"input"=>''));
