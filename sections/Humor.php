<?php require '../assets/php/conection.php'; ?>
<?php

header('Content-Type: text/html; charset=utf-8');

$category = $_POST['category'];
$subcategory = $_POST['subcategory'];
$contador = $_POST['contador'];
$cantidad = $_POST['cantidad'];
$stmt = $oConni->prepare("SELECT TITULO,RESUMEN,RUTA_IMAGEN FROM HUMOR WHERE ID_CATEGORIA = (SELECT ID_CATEGORIA FROM HUMOR_CATEGORIA WHERE NOMBRE = ?) ORDER BY ID_HUMOR DESC LIMIT ?,?");
$stmt->bind_param("sii", $subcategory, $contador, $cantidad);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($titulo, $resumen, $ruta_imagen);
$cont = 0;
$vacio = true;
if ($stmt->num_rows > 0)
      $vacio = false;
$html = '<div id="subject" data-id="' . $category . '-' . $subcategory . '">'; //Abrimos div SUBJECT
if ($subcategory == "Visual") { //Si es de la subcategoria visual solo sacamos la imagenes sin contenedor
      $html.='<ul class="rslides">';
}
while ($stmt->fetch()) {
      if ($subcategory == "Visual") { //Si es de la subcategoria visual solo sacamos la imagenes sin contenedor
            if ($ruta_imagen != '') {
                  $html.= '<li><img src="assets/images/' . $ruta_imagen . '" alt=""></li>';
                  $cont++;
            }
      } else {
            $html .='<div class="elemento animate">'; //Abrimos div  HUMOR
            if ($titulo != '') {
                  $html.='<div class="divTitulo"><div>' . utf8_decode($titulo) . '</div></div>'; //Div TITULO
            }
            $html.='<br>';

            if ($ruta_imagen != '') {
                  $html .= '<div class="divImagen"><img src="assets/images/' . $ruta_imagen . '" class="img"></div>'; //Div IMAGEN
            }
            if ($resumen != '') {
                  $html .='<div class="divResumen">' . nl2br(utf8_decode($resumen)) . '</div>'; //Div RESUMEN
            }
            $html.='<br></div>'; //Cerramos div HUMOR
      }
}
if ($subcategory == "Visual") { //Si es de la subcategoria visual solo sacamos la imagenes sin contenedor
      $html.='</ul>';
}
$html.='</div>';
$stmt->close();

$stmt = $oConni->prepare("SELECT * FROM HUMOR WHERE ID_CATEGORIA IN (SELECT ID_CATEGORIA FROM HUMOR_CATEGORIA WHERE NOMBRE LIKE ?)");
$stmt->bind_param("s", $subcategory);
$stmt->execute();
$stmt->store_result();
$n = $stmt->num_rows;

if ($subcategory != "Visual") { //Si es de la subcategoria visual solo sacamos la imagenes sin contenedor
      $orden = "mostrarMas('" . $category . "','" . $subcategory . "'," . ($contador + $cantidad) . ")";
      if ($n > ($contador + $cantidad)) {
            $input = '<input type="button" id="mas" name="mas" class="animate" onclick="' . $orden . '" value="Mostrar mas resultados..."/>';
      } else {
            $input = '';
      }
} else {
      $input = '';
}


if ($vacio) {
      $html = '<div class="elemento animate"><div class="divTitulo"><strong>No hay resultados</strong></div></div>';
      $input = '';
}
$stmt->close();

echo json_encode(array("html" => $html, "input" => $input, "cont" => $cont));
