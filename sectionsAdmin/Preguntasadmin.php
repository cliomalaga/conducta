<?php require '../assets/php/conection.php'; ?>
<?php

header('Content-Type: text/html; charset=utf-8');

$category = $_POST['category'];
$contador = $_POST['contador'];
$cantidad = $_POST['cantidad'];

$stmt = $oConni->prepare("SELECT * FROM PREGUNTAS_CATEGORIA ORDER BY ID_CATEGORIA");
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($id_categoria, $nombre);

$select = '<label for="selCategorias">Categoría:</label><select id="selCategorias" name="selCategorias">';
while ($stmt->fetch()) {
      $select .= '<option value="' . $id_categoria . '">' . $nombre . '</option>';
}
$select .= '</select><br>';
$stmt->close();

$stmt = $oConni->prepare("SELECT ID_PREGUNTAS,TITULO,RESPUESTA FROM PREGUNTAS ORDER BY ID_PREGUNTAS DESC LIMIT ?,?");
$stmt->bind_param("ii", $contador, $cantidad);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($id_preguntas, $titulo, $respuesta);

$form = '<div id="subject" data-id="' . $category . '"><div class="elemento animate">';
$form.='<form id="formulario" name="formulario" method="POST" enctype="multipart/form-data">';
$form.=$select;
$form.='<label for="txtTitulo">Titulo:</label><input type="text" id="txtTitulo" name="txtTitulo" placeholder="Título"/><br>';
$form.='<label for="txtRespuesta">Respuesta:</label><textarea id="txtRespuesta" name="txtRespuesta" placeholder="Respuesta"></textarea><br>';
$form.='<input type="button" id="butAdd" value="Añadir">';
$form.='</div></div>';

$vacio = true;
if ($stmt->num_rows > 0)
      $vacio = false;
$html = '<div id="subject" data-id="' . $category . '">'; //Abrimos div SUBJECT

while ($stmt->fetch()) {
      $html.='<div  id="pregunta' . $id_preguntas . '" class="elemento animate">';
      $html.='<p class="titlePregunta" data-id="' . $id_preguntas . '">';
      if ($titulo != '') {
            $html .= '<img id="arrow' . $id_preguntas . '" class="arrows" src="assets/img/mas.png"/> ' . utf8_decode($titulo) . '';
      }
      $html.='<input type="image" id="eliminar" name="eliminar" src="assets/img/eliminar.png" value="" class="eliminar thumbs"/>';
      $html.='<input type="image" id="editar" name="editar" src="assets/img/editar.png" value="" class="editar thumbs"/></p>';
      if ($respuesta != '') {
            $html .= '<div class="divRespuesta" style="display:none;"><br>' . nl2br(utf8_decode($respuesta)) . '</div>';
      }

      $html.='</div>';
}
$html.='</div>';
$stmt->close();

$stmt = $oConni->prepare("SELECT * FROM PREGUNTAS");
$stmt->execute();
$stmt->store_result();
$n = $stmt->num_rows;

$orden = "mostrarMas('$category'," . ($contador + $cantidad) . ")";
if ($n > ($contador + $cantidad)) {
      $input = '<input type="button" id="mas" name="mas" class="animate" onclick="' . $orden . '" value="Mostrar mas resultados..."/>';
} else {
      $input = '';
}
if ($vacio) {
      $html = '<div class="elemento animate"><div class="divTitulo"><strong>No hay resultados</strong></div></div>';
      $input = '';
}
$stmt->close();

echo json_encode(array("form" => $form, "html" => $html, "input" => $input));
