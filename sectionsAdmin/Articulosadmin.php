<?php require '../assets/php/conection.php'; ?>
<?php

header('Content-Type: text/html; charset=utf-8');

$category = $_POST['category'];
$contador = $_POST['contador'];
$cantidad = $_POST['cantidad'];

$stmt = $oConni->prepare("SELECT * FROM ARTICULO_CATEGORIA");
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($id_categoria, $nombre);

$select = '<label for="selCategorias">Categoría:</label><select id="selCategorias" name="selCategorias">';
while ($stmt->fetch()) {
      $select .= '<option value="' . $id_categoria . '">' . $nombre . '</option>';
}
$select .= '</select>';
$stmt->close();

$stmt = $oConni->prepare("SELECT ID_ARTICULO,TITULO,AUTOR,FECHA_PUBLICACION,RESUMEN,RUTA_IMAGEN,RUTA_PDF,REFERENCIA,RUTA_REFERENCIA FROM ARTICULO ORDER BY ID_ARTICULO DESC LIMIT ?,?");
$stmt->bind_param("ii", $contador, $cantidad);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($id, $titulo, $autor, $fecha, $resumen, $ruta_imagen, $ruta_pdf, $referencia, $ruta_referencia);

$monthNames = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
$form = '<div id="subject" data-id="' . $category . '"><div class="elemento animate">';
$form.='<form id="formulario" name="formulario" method="POST" enctype="multipart/form-data" accept-charset="UTF-8">';
$form.=$select;
$form.='<div class="divInputFile"><span>Subir Imagen</span><input type="file" id="subirImagen" name="subirImagen" class="upload"/></div>';
$form.='<div class="divInputFile"><span>Subir PDF</span><input type="file" id="subirPDF" name="subirPDF" class="upload"/></div><br>';
$form.='<div id="divImgPrevia"></div>';
$form.='<label for="txtTitulo">Titulo:</label><input type="text" id="txtTitulo" name="txtTitulo" placeholder="Título"/><br>';
$form.='<label for="txtAutor">Autor:</label><input type="text" id="txtAutor" name="txtAutor" placeholder="Autor"/><br>';
$form.='<label for="txtFecha">Fecha de publicación:</label><input type="text" id="txtFecha" name="txtFecha" placeholder="DD/MM/YYYY"><br>';
$form.='<label for="txtReferencia">Referencia:</label><input type="text" id="txtReferencia" name="txtReferencia" placeholder="Referencia"/><br>';
$form.='<label for="txtRutaReferencia">Ruta Referencia:</label><input type="text" id="txtRutaReferencia" name="txtRutaReferencia" placeholder="http://www.google.es"/><br>';
$form.='<label for="txtResumen">Resumen:</label><textarea id="txtResumen" name="txtResumen" placeholder="Resumen"></textarea>';
$form.='<input type="button" id="butAdd" value="Añadir">';
$form.='</form></div></div>';

$vacio = true;
if ($stmt->num_rows > 0)
      $vacio = false;
$html = '<div id="subject" data-id="' . $category . '">'; //Abrimos div SUBJECT
while ($stmt->fetch()) {
      $html .='<div class="elemento animate">'; //Abrimos div ARTICULOS
      $html.='<div class="divTitulo" data-id="' . $id . '">';
      if ($titulo != '') {
            $html.= '<div>' . utf8_decode($titulo) . '</div>';
      }
      $html.='<input type="image" id="eliminar" name="eliminar" src="assets/img/eliminar.png" value="" class="eliminar thumbs"/>';
      $html.='<input type="image" id="editar" name="editar" src="assets/img/editar.png" value="" class="editar thumbs"/>';
      $html.='</div>';

      $html .='<div class="divAutor">'; //Abrimos div AUTOR
      if ($autor != '') {
            $html.='<strong>Autor:</strong> ' . utf8_decode($autor);
      }

      if ($fecha != '') {
            $month = (substr($fecha, 3, 2) - 1);
            $fecha = substr($fecha, 0, 2) . ' de ' . $monthNames[$month] . ' de ' . substr($fecha, 6, 4);
            $html.='<br>Publicado el ' . $fecha;
      }

      if ($referencia != '' && $ruta_referencia != '') { //Div REFERENCIA
            $html .=' - <a href="http://' . $ruta_referencia . '" target="_blank">' . utf8_decode($referencia) . '</a>';
      }
      $html.='</div><br>'; //Cerramos div AUTOR

      if ($ruta_imagen != '') {//Div IMAGEN
            $html .= '<div class="divImagen"><img src="' . DIRECTORIOIMAGEN . $ruta_imagen . '" class="img"></div>';
      }

      if ($resumen != '') {
            $html .='<div class="divResumen">' . nl2br(utf8_decode($resumen)) . '</div>'; //Div RESUMEN
      }

      if ($ruta_pdf != '') {
            $html.='<div class="divLeer"><a class="leerMas" name="' . DIRECTORIOPDF . $ruta_pdf . '" title="Leer más sobre ' . $ruta_pdf . '">Leer más...</a></div>';
      }
      $html.='<br></div>'; //Cerramos div ARTICULOS
}
$html.='</div>';
$stmt->close();

$stmt = $oConni->prepare("SELECT * FROM ARTICULO");
$stmt->execute();
$stmt->store_result();
$n = $stmt->num_rows;

$orden = "mostrarMas('" . $category . "'," . ($contador + $cantidad) . ")";
if ($n > ($contador + $cantidad)) {
      $input = '<input type="button" id="mas" name="mas" class="animate" onclick="' . $orden . '" value="Mostrar mas resultados..."/>';
} else {
      $input = '';
}
if ($vacio) {
      $html = '<div class="elemento animate"><div class="divTitulo"><strong>No hay resultados</strong></div></div>';
      $input = '';
}
$stmt->close();
echo json_encode(array("form" => $form, "html" => $html, "input" => $input));
