<?php require('assets/php/validation.php'); ?>
<html>
      <head>
            <title>CONTEXTOS - Administración</title>
            <meta charset="utf-8" />
            <meta name=viewport content="width=device-width, initial-scale=1">
            <link type="text/css" rel="stylesheet" href="assets/css/normalize.css" />
            <link type="text/css" rel="stylesheet" href="assets/css/animate.css" />
            <link type="text/css" rel="stylesheet" href="assets/css/hiddenDivs.css" />
            <link type="text/css" rel="stylesheet" href="assets/css/main.css" />
            <link type="text/css" rel="stylesheet" href="assets/css/admin.css" />
            <link type="text/css" rel="stylesheet" href="assets/css/adminForms.css" />
            <link type="text/css" rel="stylesheet" href="assets/css/hiddenDivs.css" />




            <link rel="Contextos Icon" href="assets/img/logo.png">
      </head>
      <body>
            <header>
                  <div id="divBanner">
                        <img class="fotoBanner" src="assets/img/background-banner.png" alt="Banner1"/>
                  </div>
                  <a href="http://5.135.145.180/admin.php">
                        <p id="title">ADMINISTRACIÓN</p>
                  </a>
            </header>
            <nav id="divButtons">
                  <ul class="main">
                        <li><a>Noticias</a></li>
                        <li><a>Artículos</a></li>
                        <li><a>Libros</a></li>
                        <li><a>Vida cotidiana</a></li>
                        <li><a>Preguntas</a></li>
                        <li><a>Bibliografía</a></li>
                        <li><a>Humor</a></li>
                  </ul>
            </nav>
            <div id="divContent">
                  <main>
                        <section>
                              <span id="pBienvenida">
                                    ¡Bienvenido a la Administración de Contextos,  <?php echo ucwords($_COOKIE["nombre"]); ?>!
                              </span>
                        </section>
                  </main>
                  <footer>
                        <p id="footer">
                              <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">
                                    <img alt="Licencia de Creative Commons" src="assets/img/cc.png" />
                              </a>
                              <span >Contextos is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Attribution 4.0 Internacional License.</a></span><br>
                              <span>Copyright © <?php echo date('Y'); ?> Gabriel Valero & Iván Gómez Design  All rights reserved.</span>
                        </p>
                  </footer>
            </div>
            <div id="divOscuro"></div>
            <div id="divPdf"></div>
            <div id="divModal"> </div>

            <!-- SCRIPT ONLOAD -->
            <script type="text/javascript" src="assets/js/modernizr.custom.28468.js"></script>
            <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
            <script>
                  window.jQuery || document.write(' <script src="assets/js/js original/jquery-1.11.1.min"><\/script>')
            </script>
            <script type="text/javascript" src="assets/js/admin.js"></script>
      </body>
</html>