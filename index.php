<!DOCTYPE>
<html>
	<head>
		<title>CONTEXTOS - Conducta.org</title>
		<meta charset="utf-8" />
		<meta name="description" content="Esta es la página de Contextos. Artículos, noticias, preguntas sobre el análisis de conducta">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<link type="text/css" rel="stylesheet" href="assets/css/normalize.css" />
		<link type="text/css" rel="stylesheet" href="assets/css/animate.css" />
		<link type="text/css" rel="stylesheet" href="assets/css/hiddenDivs.css" />
		<link type="text/css" rel="stylesheet" href="assets/css/main.css" />
		<link type="text/css" rel="stylesheet" href="assets/css/slider.css" />

		<link type="text/css" rel="stylesheet" href="assets/css/index.css" />
		<link type="text/css" rel="stylesheet" href="assets/css/index-ie.css" />
		<link type="text/css" rel="stylesheet" href="assets/css/nosotros.css" />
		<link type="text/css" rel="stylesheet" href="assets/css/checkbox.css" />
		<link type="text/css" rel="stylesheet" href="assets/css/hiddenDivs.css" />
		<link rel="Contextos Icon" href="assets/img/logo.png">

		<script>
			(function (i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] || function () {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
				a = s.createElement(o),
					m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m)
			})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

			ga('create', 'UA-62617983-1', 'auto');
			ga('send', 'pageview');

		</script>

	</head>
	<body>
		<header>
			<div id="divBanner">
				<img class="fotoBanner" src="assets/img/background-banner.png" alt="Banner de Contextos"/>
			</div>
			<a href="http://www.conducta.org">
				<p id="logo"> <img id="imgLogo" src="assets/img/logo.png" alt="Logo de Contextos en banner"/></p>
				<p id="title">Contextos</p>
				<p id="subtitle">conducta.org</p>
			</a>
		</header>
		<nav id="divButtons">
			<ul class="main">
				<li>
					<a>Noticias</a>
					<ul>
						<li><a>Congresos</a></li>
						<li><a>Jornadas</a></li>
						<li><a>Cursos</a></li>
						<li><a>Revistas</a></li>
						<li><a>Internet</a></li>
					</ul>
				</li>
				<li>
					<a>Artículos</a>
					<ul>
						<li><a>Divulgación</a></li>
						<li><a>Críticos</a></li>
						<li><a>Conductismos</a></li>
						<li><a>Conceptos</a></li>
						<li><a>Metodológicos</a></li>
						<li><a>Tratamientos</a></li>
						<li><a>Psicoterapias</a></li>
						<li><a>Aplicaciones Diversas</a></li>
						<li><a>3º Jornadas</a></li>
					</ul>
				</li>
				<li>
					<a>Libros</a>
					<ul>
						<li><a>Revisiones</a></li>
						<li><a>Libros Completos</a></li>
						<li><a>Biografías</a></li>
					</ul>
				</li>
				<li>
					<a>Vida cotidiana</a>
					<ul>
						<li><a>Educación</a></li>
						<li><a>Ecología</a></li>
						<li><a>Violencia</a></li>
						<li><a>Política</a></li>
						<li><a>Televisión</a></li>
						<li><a>Curiosidades</a></li>
					</ul>
				</li>
				<li>
					<a>Preguntas</a>
					<ul>
						<li><a>Teoría del Conductismo</a></li>
						<li><a>Experimentación</a></li>
						<li><a>Conductas Clínicas</a></li>
						<li><a>Educación</a></li>
						<li><a>Conducta Social</a></li>
						<li><a>Conducta y Salud</a></li>
						<li><a>Otras Preguntas</a></li>
					</ul>
				</li>
				<li>
					<a>Bibliografía</a>
					<ul>
						<li><a>Conductismo</a></li>
						<li><a>Modificación Conducta</a></li>
						<li><a>Análisis Funcional</a></li>
						<li><a>Análisis Aplicado</a></li>
						<li><a>Experimentación Animal</a></li>
						<li><a>Psicoterapias 3º Generación</a></li>
						<li><a>Conducta y Salud</a></li>
						<li><a>Más Enlaces</a></li>
					</ul>
				</li>
				<li>
					<a>Humor</a>
					<ul>
						<li><a>Visual</a></li>
						<li><a>Analistas de Conducta</a></li>
						<li><a>Psicólogos</a></li>
						<li><a>Otros chistes</a></li>
					</ul>
				</li>
				<li>
					<a>Nosotros</a>
					<ul>
						<li><a>Quienes somos</a></li>
						<li><a>Editorial</a></li>
						<li><a>Jornadas</a></li>
						<li><a>Compras</a></li>
						<li><a>Colaboraciones</a></li>
						<li><a>Autorizaciones</a></li>
						<li><a>Lista AC</a></li>
					</ul>
				</li>
				<li>
					<input type="text" id="txtBuscar" name="txtBuscar">
				</li>
			</ul>
		</nav>

		<div id="divContent">
			<main>
				<section id="sectionSection"><h1></h1>
					<div id='divIzquierda'>
						<?php include 'assets/php/indexIzquierda.php' ?>
					</div>
					<div id='divDerecha'>
						<?php include 'assets/php/indexDerecha.php' ?>
					</div>
				</section>
				<section id="sectionAdSense">
					<div id="divAdSense">
						<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							 style="display:block"
							 data-ad-client="ca-pub-8020975076791682"
							 data-ad-slot="2328053057"
							 data-ad-format="auto"></ins>
						<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
						</script>
						<!-- -->
						<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							 style="display:block"
							 data-ad-client="ca-pub-8020975076791682"
							 data-ad-slot="2328053057"
							 data-ad-format="auto"></ins>
						<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
						</script>
						<!-- -->
						<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							 style="display:block"
							 data-ad-client="ca-pub-8020975076791682"
							 data-ad-slot="2328053057"
							 data-ad-format="auto"></ins>
						<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
						</script>
					</div>
				</section>
			</main>
			<footer>
				<div id="footer">
					<a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">
						<img alt="Licencia de Creative Commons" src="assets/img/cc.png" />
					</a>
					<div>Contextos is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Attribution 4.0 Internacional License.</a></div>
					<div>Copyright © <?php echo date('Y'); ?> <a href="https://es.linkedin.com/in/gabrielvaleroperez">Gabriel Valero</a> & Iván Gómez Web Design.  All rights reserved.</div>
				</div>
			</footer>
		</div>
		<div id="divComentario">
			<h1><span>Consúltanos tu pregunta</span></h1>
			<div id="divFormComentario">
				<div id="inputsComentario">
					<label>Nombre: </label><input type="text" placeholder="Escriba su nombre" id="txtNombre" onkeyup="validarFormPreguntas(), pintarNombre()" onblur="" value="Gabriel"><br>
					<label>Correo electrónico: </label><input type="text" placeholder="Escriba su e-mail" id="txtEmail" onkeyup="validarFormPreguntas(), pintarEmail()" onblur="" value="gabrielvperez@gmail.com"><br>
				</div>
				<div id="warningComentario">
					<p id="warningNombre"></p>
					<p id="warningCorreo"></p>
				</div>
			</div>
			<textarea id="texto"></textarea><br>
			<div class="check">
				<input type="checkbox" id="checkAceptar" onclick="validarFormPreguntas()"><label for="checkAceptar"></label>
				<span>Acepto la publicación anónima en la web de mi pregunta.</span><br>
			</div>
			<input type="button" value="Enviar" id="butEnviarMail" disabled>
			<input type="button" value="Cancelar" id="butCancelar" class="cancelar">
		</div>
		<div id="divOscuro"></div>
		<div id="divPdf"></div>
		<div id="divModal"> </div>


		<!-- SCRIPT ONLOAD -->
		<script type="text/javascript" src="assets/js/modernizr.custom.28468.js"></script>
		<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
		<script> window.jQuery || document.write(' <script src="assets/js/js original/jquery-1.11.1.min"><\/script>')</script>
		<script type="text/javascript" src="assets/js/index.js"></script>
		<script type="text/javascript" src="assets/js/mail.js"></script>
		<script type="text/javascript"  src="assets/js/slider.js"></script>
		<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
		<link rel="stylesheet" type="text/css" href="assets/css/CookieConsent.css"/>
		<script type="text/javascript" src="assets/js/CookieConsent.js"></script>
		<script type="text/javascript">
						// <![CDATA[
						cc.initialise({
							cookies: {
								analytics: {
									title: 'Google Analytics',
									description: 'Anónimamente medimos su uso en' +
										' esta web, para mejorar su exp' +
										'eriencia.'
								},
								advertising: {
									title: 'Publicidad',
									description: 'Los anuncios serán elegidos de' +
										' forma automática en base a su' +
										' comportamiento e intereses.'
								}
							},
							settings: {
								consenttype: "implicit",
								bannerPosition: "bottom",
								hideprivacysettingstab: true
							},
							strings: {
								socialDefaultDescription: 'Facebook, Twitter y otros sitios web sociales necesitan saber quién eres para que funcione, correctamente.',
								analyticsDefaultDescription: 'Anónimamente medimos su uso de este sitio web para mejorar s,u experiencia.',
								advertisingDefaultTitle: 'Publicidad',
								advertisingDefaultDescription: 'Los anuncios serán elegidos de forma automática en base a su comportamiento e intereses.',
								necessaryDefaultTitle: 'Estrictamente necesario',
								necessaryDefaultDescription: 'Algunas cookies en este sitio web son estrictamente necesarias y no se pueden desactivar.',
								defaultTitle: 'Cookie',
								defaultDescription: 'Cookie descripción.',
								learnMore: 'Leer más',
								closeWindow: 'Cerrar ventana',
								notificationTitle: 'Su experiencia en este sitio se mejorará al permitir las cookies',
								notificationTitleImplicit: 'Utilizamos cookies para asegurarse de obtener la mejor experiencia en nuestro sitio web',
								customCookie: 'Esta página web utiliza un tipo personalizado de cookie que necesita la aprobación específica',
								seeDetails: 'Ver detalles',
								seeDetailsImplicit: 'cambiar tus preferencias',
								hideDetails: 'ocultar detalles',
								allowCookies: 'Permitir cookies',
								allowCookiesImplicit: 'Cerrar',
								allowForAllSites: 'Permitir para todos los sitios',
								savePreference: 'Guardar preferencias',
								saveForAllSites: 'Guardar para todos los sitios',
								privacySettings: 'Configuración de privacidad',
								privacySettingsDialogTitleA: 'Configuración de privacidad',
								privacySettingsDialogTitleB: 'Para esta web',
								changeForAllSitesLink: 'Cambiar la configuración para ,todos los sitios web',
								privacySettingsDialogSubtitle: 'Algunas funciones de este sitio web es necesario su consentimiento para recordar quién eres.',
								preferenceUseGlobal: 'Usar configuración global',
								preferenceConsent: 'Doy mi consentimiento',
								preferenceDecline: 'Declino',
								notUsingCookies: 'Este sitio web no utiliza cookies.',
								allSitesSettingsDialogTitleA: 'La configuración de privacidad',
								allSitesSettingsDialogTitleB: 'Para todos los sitios web',
								allSitesSettingsDialogSubtitle: 'Usted puede dar su consentimiento a estas cookies para todos los sitios web que utilizan este plugin.',
								backToSiteSettings: 'Volver a la configuración del sitio web',
								preferenceAsk: 'Preguntar cada vez',
								preferenceAlways: 'Permitir siempre',
								preferenceNever: 'Nunca permita'
							}
						});
						// ]]>
		</script>
		<!-- End Cookie Consent plugin -->
	</body>
</html>